"""API inspect client and models definitions."""
import json
import logging
from datetime import datetime
from ipaddress import IPv4Network, IPv6Network
from typing import Any, Dict, List, Optional, Union
from uuid import UUID

import requests
import tenacity
from pydantic import BaseModel, IPvAnyNetwork, root_validator

from dt_api_security_results.models.assets import (
    CloudResource,
    DiscoveredViaEnum,
    DomainName,
    IpRange,
    NetworkService,
    OpenApiDefinition,
    RestfulApi,
    WebApplication,
)
from dt_api_security_results.models.cloud_authenticators import (
    ApigeeAuthenticatorResponse,
    ApigeeCredential,
    ApigeeOnPremAuthenticatorResponse,
    ApigeeOnPremCredential,
    AwsAuthenticatorResponse,
    AwsCredential,
    AxwayAuthenticatorResponse,
    AxwayCredential,
    AzureAuthenticatorResponse,
    AzureCredential,
    CloudAuthenticatorStatusEnum,
    CloudAuthenticatorTypesEnum,
    GcpAuthenticatorResponse,
    GcpCredential,
    KongAuthenticatorResponse,
    KongCredential,
    MulesoftAuthenticatorResponse,
    MulesoftCredential,
)
from dt_api_security_results.models.policy_violations import (
    AssetTypeEnum,
    PolicyViolation,
    PolicyViolationAffectingAsset,
    PolicyViolationStatusEnum,
    ProductEnum,
    SeverityEnum,
)

_logger = logging.getLogger(__name__)


class PaginationInformationField(BaseModel):
    total_count: int
    next_cursor: Optional[str]  # None if there are no more records to fetch


class BasePaginatedResponse(BaseModel):
    pagination_information: PaginationInformationField


class CloudResourceGetResponse(BaseModel):
    """Response from cloud_resources_get"""

    cloud_resources: List[CloudResource]


class CloudResourceListResponse(BasePaginatedResponse):
    """Response from cloud_resources_list"""

    cloud_resources: List[CloudResource]


class IpRangeCreateRequest(BaseModel):
    ip_range: IPvAnyNetwork


class IpRangeGetResponse(BaseModel):
    """Response from ip_ranges_get"""

    ip_ranges: List[IpRange]


class IpRangeListResponse(BasePaginatedResponse):
    """Response from ip_ranges_list"""

    ip_ranges: List[IpRange]


class NetworkServiceGetResponse(BaseModel):
    """Response from network_services_get"""

    domain_names: List[DomainName]
    network_services: List[NetworkService]


class NetworkServiceListResponse(BasePaginatedResponse):
    """Response from network_services_list"""

    domain_names: List[DomainName]
    network_services: List[NetworkService]


class PolicyViolationGetResponse(PolicyViolation):
    """Response from policy_violations_get"""


class PolicyViolationListResponse(BasePaginatedResponse):
    """Response from policy_violations_list"""

    # The actual policy violations
    policy_violations: List[PolicyViolation]


class PolicyViolationAffectingAssetListResponse(BasePaginatedResponse):
    """Response from policy_violations_list"""

    # The actual policy violations
    policy_violations: List[PolicyViolationAffectingAsset]


class AssetGroupField(BaseModel):
    id: UUID
    name: str


class AssetTagAppliedToAssetField(BaseModel):
    id: UUID
    tag: str
    tag_id: UUID
    value: Optional[str] = None
    imported_from: Optional[str] = None
    imported_external_id: Optional[str] = None
    imported_from_icon_url: Optional[str] = None


class AssetSummaryField(BaseModel):
    id: UUID
    name: str
    url: Optional[str]
    belongs_to_asset_group: Optional[AssetGroupField]
    asset_type: str
    asset_type_icon_url: str
    asset_type_name: str

    open_urgent_policy_violations_count: int
    open_important_policy_violations_count: int
    open_proactive_policy_violations_count: int

    discovered_via: str
    discovered_via_icon_url: str
    discovered_via_name: str

    hosted_on: str
    hosted_on_icon_url: str
    hosted_on_name: str

    status: str
    date_created: datetime
    date_no_longer_accessible: Optional[datetime]

    tags: List[AssetTagAppliedToAssetField]


class AssetsListResponse(BasePaginatedResponse):
    assets: List[AssetSummaryField]


class RestfulApiGetResponse(BaseModel):
    """Response from restful_apis_get"""

    restful_apis: List[RestfulApi]
    domain_names: List[DomainName]
    network_services: List[NetworkService]


class RestfulApiListResponse(BasePaginatedResponse):
    """Response from restful_apis_list"""

    restful_apis: List[RestfulApi]
    domain_names: List[DomainName]
    network_services: List[NetworkService]


class WebApplicationGetResponse(BaseModel):
    """Response from web_applications_get"""

    web_applications: List[WebApplication]
    domain_names: List[DomainName]
    network_services: List[NetworkService]


class WebApplicationListResponse(BasePaginatedResponse):
    """Response from web_applications_list"""

    web_applications: List[WebApplication]
    domain_names: List[DomainName]
    network_services: List[NetworkService]


class OpenApiDefinitionsResponse(BasePaginatedResponse):
    openapi_definitions: List[OpenApiDefinition]


class OpenApiDefinitionCreateResponse(BaseModel):
    id: UUID
    network_services: List[NetworkService]
    domain_names: List[DomainName]
    restful_apis: List[RestfulApi]


class CloudAuthenticatorCreateRequest(BaseModel):
    cloud_authenticator_type: CloudAuthenticatorTypesEnum
    apigee_credential: Optional[ApigeeCredential]
    apigee_on_prem_credential: Optional[ApigeeOnPremCredential]
    aws_credential: Optional[AwsCredential]
    axway_credential: Optional[AxwayCredential]
    azure_credential: Optional[AzureCredential]
    gcp_credential: Optional[GcpCredential]
    kong_credential: Optional[KongCredential]
    mulesoft_credential: Optional[MulesoftCredential]

    @root_validator
    def validate_cloud_authenticator_type(cls, values: Dict[str, Any]) -> Dict[str, Any]:
        # Used to check and retrieve the expected credential from a CloudAuthenticatorTypesEnum
        _authenticator_credentials_mapping = {
            CloudAuthenticatorTypesEnum.APIGEE: "apigee_credential",
            CloudAuthenticatorTypesEnum.APIGEE_ON_PREM: "apigee_on_prem_credential",
            CloudAuthenticatorTypesEnum.AMAZON_WEB_SERVICES: "aws_credential",
            CloudAuthenticatorTypesEnum.AXWAY: "axway_credential",
            CloudAuthenticatorTypesEnum.AZURE: "azure_credential",
            CloudAuthenticatorTypesEnum.GOOGLE_CLOUD_PLATFORM: "gcp_credential",
            CloudAuthenticatorTypesEnum.KONG: "kong_credential",
            CloudAuthenticatorTypesEnum.MULESOFT: "mulesoft_credential",
        }
        credential_data_key = _authenticator_credentials_mapping[values["cloud_authenticator_type"]]
        if values.get(credential_data_key) is None:
            raise ValueError(f"Expected \"{credential_data_key}\" for {values.get('cloud_authenticator_type')}.")
        return values


class CloudAuthenticatorCreateResponse(BaseModel):
    status: CloudAuthenticatorStatusEnum
    apigee_credential: Optional[ApigeeAuthenticatorResponse]
    apigee_on_prem_credential: Optional[ApigeeOnPremAuthenticatorResponse]
    aws_credential: Optional[AwsAuthenticatorResponse]
    azure_credential: Optional[AzureAuthenticatorResponse]
    gcp_credential: Optional[GcpAuthenticatorResponse]
    kong_credential: Optional[KongAuthenticatorResponse]
    mulesoft_credential: Optional[MulesoftAuthenticatorResponse]
    axway_credential: Optional[AxwayAuthenticatorResponse]
    authenticator_type: CloudAuthenticatorTypesEnum


class ApiKeyAuth(requests.auth.AuthBase):
    def __init__(self, api_key: str) -> None:
        self.api_key = api_key

    def __call__(self, request: requests.PreparedRequest) -> requests.PreparedRequest:
        request.headers["Authorization"] = f"APIKey {self.api_key}"
        return request


class ApiSecurityResultsClient:
    """Python client for Data Theorem's API Security Results API.

    In order to use it, you must provide an API Key configured in Data Theorem's portal, that has been granted access to
    the API Security Results API.

    Pagination:
        Several of the API operations/collection resources/list methods are paginated. This means that they return a
        single "page" of results at a time, and they may require several calls in order to fetch all of the
        resources in the collection or that match your query.

    Example Usage:

        # The following example shows how to fetch all URGENT policy violations
        client = APIInspectClient(my_api_key)

        policy_violation_responses = []
        policy_violations = []
        current_response = client.policy_violations_list(filter_by_relevance="URGENT")
        policy_violation_responses.append(current_response)
        while current_response.pagination_information.next_cursor:
            current_response = client.policy_violations_list(
                filter_by_relevance="URGENT",
                cursor=current_response.pagination_information.next_cursor
            )
            policy_violation_responses.append(current_response)
            policy_violations.extend(current_response.policy_violations)

        # Process the policy violations or other data in their responses
    """

    def __init__(self, api_key: str, base_url: str = "https://api.securetheorem.com/apis/api_security/results") -> None:
        """
        Args:
            api_key: The API Key used to authenticate the client to the API Security Results API.
            base_url: The base URL for the API. It defaults to the publicly accessible version of the API.
        """
        self.api_key = api_key
        self.base_url = base_url

    @property
    def _auth(self) -> ApiKeyAuth:
        return ApiKeyAuth(self.api_key)

    def _url_for(self, endpoint: str, api_version: str = "v1beta1") -> str:
        return f"{self.base_url}/{api_version}{endpoint}"

    @tenacity.retry(stop=tenacity.stop_after_attempt(5), after=tenacity.after_log(_logger, logging.INFO))
    def cloud_resources_get(self, id: UUID) -> CloudResourceGetResponse:
        """Returns a single CloudResource in a CloudResourceGetResponse.

        Args:
            id: The ID of the cloud resource.

        Returns:
            CloudResourceGetResponse containing the cloud resource of the supplied id.

        Raises:
            pydantic.ValidationError: A malformed response was returned.
            requests.RequestException: A connection or an HTTP error occurred.
        """
        url = self._url_for(f"/cloud_resources/{id}")
        response = requests.get(url, auth=self._auth)
        response.raise_for_status()
        return CloudResourceGetResponse.parse_raw(response.text)

    @tenacity.retry(stop=tenacity.stop_after_attempt(5), after=tenacity.after_log(_logger, logging.INFO))
    def cloud_resources_list(self, *, cursor: Optional[str] = None) -> CloudResourceListResponse:
        """Returns a page of CloudResources in a CloudResourceListResponse.

        This resource collection is paginated. Take a look at the examples to understand how to handle pagination.

        Args:
            cursor: Optional cursor to request the next page.

        Returns:
            CloudResourceListResponse containing one page of the cloud resources in your account.

        Raises:
            pydantic.ValidationError: A malformed response was returned.
            requests.RequestException: A connection or an HTTP error occurred.

        """
        url = self._url_for("/cloud_resources")

        query_parameters = {}
        if cursor:
            query_parameters["cursor"] = cursor

        response = requests.get(url, auth=self._auth, params=query_parameters)
        response.raise_for_status()
        return CloudResourceListResponse.parse_raw(response.text)

    @tenacity.retry(stop=tenacity.stop_after_attempt(5), after=tenacity.after_log(_logger, logging.INFO))
    def ip_ranges_get(self, id: UUID) -> IpRangeGetResponse:
        """Returns a single IpRange in an IpRangeGetResponse.

        Args:
            id: The ID of the IP range.

        Returns:
            IpRangeGetResponse containing the IP range of the supplied id.

        Raises:
            pydantic.ValidationError: A malformed response was returned.
            requests.RequestException: A connection or an HTTP error occurred.
        """
        url = self._url_for(f"/ip_ranges/{id}")
        response = requests.get(url, auth=self._auth)
        response.raise_for_status()
        return IpRangeGetResponse.parse_raw(response.text)

    @tenacity.retry(stop=tenacity.stop_after_attempt(5), after=tenacity.after_log(_logger, logging.INFO))
    def ip_ranges_list(self, *, cursor: Optional[str] = None) -> IpRangeListResponse:
        """Returns a page of IpRange in an IpRangeListResponse.

        This resource collection is paginated. Take a look at the examples to understand how to handle pagination.

        Args:
            cursor: Optional cursor to request the next page.

        Returns:
            IpRangeListResponse containing one page of the IP ranges in your account.

        Raises:
            pydantic.ValidationError: A malformed response was returned.
            requests.RequestException: A connection or an HTTP error occurred.

        """
        url = self._url_for("/ip_ranges")

        query_parameters = {}
        if cursor:
            query_parameters["cursor"] = cursor

        response = requests.get(url, auth=self._auth, params=query_parameters)
        response.raise_for_status()
        return IpRangeListResponse.parse_raw(response.text)

    def ip_ranges_create(self, ip_range: Union[IPv4Network, IPv6Network]) -> IpRangeGetResponse:
        """Create a new IP range for continuous discovery.

        Args:
            ip_range: The IP range to create.

        Returns:
            IpRangeGetResponse containing the newly created IP range.

        Raises:
            pydantic.ValidationError: A malformed response was returned.
            requests.RequestException: A connection or an HTTP error occurred.

        """
        url = self._url_for("/ip_ranges")
        response = requests.post(
            url,
            headers={"Content-Type": "application/json"},
            auth=self._auth,
            data=IpRangeCreateRequest(ip_range=ip_range).json(),
        )
        response.raise_for_status()
        return IpRangeGetResponse.parse_raw(response.text)

    @tenacity.retry(stop=tenacity.stop_after_attempt(5), after=tenacity.after_log(_logger, logging.INFO))
    def network_services_get(self, id: UUID) -> NetworkServiceGetResponse:
        """Returns a single NetworkService (and any associated DomainNames) in a NetworkServiceGetResponse.

        Args:
            id: The ID of the network service.

        Returns:
            NetworkServiceGetResponse containing the network service with the supplied id, as well as any associated
            DomainNames.

        Raises:
            pydantic.ValidationError: A malformed response was returned.
            requests.RequestException: A connection or an HTTP error occurred.
        """
        url = self._url_for(f"/network_services/{id}")
        response = requests.get(url, auth=self._auth)
        response.raise_for_status()
        return NetworkServiceGetResponse.parse_raw(response.text)

    @tenacity.retry(stop=tenacity.stop_after_attempt(5), after=tenacity.after_log(_logger, logging.INFO))
    def network_services_list(self, *, cursor: Optional[str] = None) -> NetworkServiceListResponse:
        """Returns a page of NetworkServices in a NetworkServiceListResponse.

        This resource collection is paginated. Take a look at the examples to understand how to handle pagination.

        Args:
            cursor: Optional cursor to request the next page.

        Returns:
            NetworkServiceListResponse containing one page of the network services in your account.

        Raises:
            pydantic.ValidationError: A malformed response was returned.
            requests.RequestException: A connection or an HTTP error occurred.

        """
        url = self._url_for("/network_services")

        query_parameters = {}
        if cursor:
            query_parameters["cursor"] = cursor

        response = requests.get(url, auth=self._auth, params=query_parameters)
        response.raise_for_status()
        return NetworkServiceListResponse.parse_raw(response.text)

    @tenacity.retry(stop=tenacity.stop_after_attempt(5), after=tenacity.after_log(_logger, logging.INFO))
    def policy_violations_get(self, id: UUID) -> PolicyViolationGetResponse:
        """Returns a particular PolicyViolation (along with associated resources) in a PolicyViolationGetResponse.

        Args:
            id: The ID of the policy violation.

        Returns:
            PolicyViolationGetResponse containing the policy violation with the supplied id, along with related data
            such as its PolicyRule, PolicyRuleType, and the asset it affects.

        Raises:
            pydantic.ValidationError: A malformed response was returned.
            requests.RequestException: A connection or an HTTP error occurred.

        """
        url = self._url_for(f"/policy_violations/{id}", api_version="v2")
        response = requests.get(url, auth=self._auth)
        response.raise_for_status()
        return PolicyViolationGetResponse.parse_raw(response.text)

    @tenacity.retry(stop=tenacity.stop_after_attempt(5), after=tenacity.after_log(_logger, logging.INFO))
    def policy_violations_list(
        self,
        *,
        cursor: Optional[str] = None,
        filter_by_severity: Optional[List[SeverityEnum]] = None,
        filter_by_violation_statuses: Optional[List[PolicyViolationStatusEnum]] = None,
        filter_by_product: Optional[List[ProductEnum]] = None,
        filter_by_asset_group_uuid: Optional[str] = None,
        was_discovered_after: Optional[datetime] = None,
    ) -> PolicyViolationListResponse:
        """Returns a page of PolicyViolations in a PolicyViolationListResponse.

        This resource collection is paginated. Take a look at the examples to understand how to handle pagination.

        Args:
            cursor: Optional cursor to request the next page.
            filter_by_severity: Filter the returned policy violations by their severity. Currently defined severity
                levels include LOW, MEDIUM, and HIGH.
            filter_by_violation_statuses: Filter the returned policy violations by their stauses,
                default is ["OPEN", "WONT_FIX"]
                if not specified. Can be any combination of OPEN WONT_FIX CLOSED DELETED
            was_discovered_after: Returns the policy violations that have been created after the specified date.
                Must be UTC time zone datetime.
        Returns:
            PolicyViolationListResponse containing one page of the policy violations in your account.

        Raises:
            pydantic.ValidationError: A malformed response was returned.
            requests.RequestException: A connection or an HTTP error occurred.
        """
        url = self._url_for("/policy_violations", api_version="v2")
        query_parameters = {}
        if cursor:
            query_parameters["cursor"] = cursor
        if filter_by_severity:
            query_parameters["filter_by_severity"] = filter_by_severity  # type: ignore
        if filter_by_product:
            query_parameters["filter_by_product"] = filter_by_product  # type: ignore
        if filter_by_asset_group_uuid:
            query_parameters["filter_by_asset_group_id"] = filter_by_asset_group_uuid
        if not filter_by_violation_statuses:
            filter_by_violation_statuses = [PolicyViolationStatusEnum("OPEN"), PolicyViolationStatusEnum("WONT_FIX")]
        query_parameters["filter_by_violation_statuses"] = filter_by_violation_statuses  # type: ignore
        if was_discovered_after:
            query_parameters["was_discovered_after"] = was_discovered_after.strftime("%Y-%m-%dT%H:%M:%SZ")

        response = requests.get(url, auth=self._auth, params=query_parameters)
        response.raise_for_status()
        return PolicyViolationListResponse.parse_raw(response.text)

    @tenacity.retry(stop=tenacity.stop_after_attempt(5), after=tenacity.after_log(_logger, logging.INFO))
    def policy_violations_affecting_asset_list(
        self, *, cursor: Optional[str] = None, id: UUID
    ) -> PolicyViolationAffectingAssetListResponse:
        """Returns a list of PolicyViolations in the AllPolicyViolationsAffectingAssetResource.

        Args:
            id: The ID of the asset.

        Returns:
            PolicyViolationGetResponse containing the policy violation with the supplied id, along with related data
            such as its PolicyRule, PolicyRuleType, and the asset it affects.

        Raises:
            pydantic.ValidationError: A malformed response was returned.
            requests.RequestException: A connection or an HTTP error occurred.

        """
        url = self._url_for(f"/assets/{id}/policy_violations", api_version="v2")
        query_parameters = {}
        if cursor:
            query_parameters["cursor"] = cursor
        response = requests.get(url, auth=self._auth, params=query_parameters)
        response.raise_for_status()
        return PolicyViolationAffectingAssetListResponse.parse_raw(response.text)

    @tenacity.retry(stop=tenacity.stop_after_attempt(5), after=tenacity.after_log(_logger, logging.INFO))
    def restful_apis_get(self, id: UUID) -> RestfulApiGetResponse:
        """Returns a particular RestfulApi in a RestfulApiGetResponse.

        Args:
            id: The ID of the RESTful API.

        Returns:
            RestfulApiGetResponse containing the RESTful API with the supplied ID

        Raises:
            pydantic.ValidationError: A malformed response was returned.
            requests.RequestException: A connection or an HTTP error occurred.
        """
        url = self._url_for(f"/restful_apis/{id}")
        response = requests.get(url, auth=self._auth)
        response.raise_for_status()
        return RestfulApiGetResponse.parse_raw(response.text)

    @tenacity.retry(stop=tenacity.stop_after_attempt(5), after=tenacity.after_log(_logger, logging.INFO))
    def restful_apis_list(self, *, cursor: Optional[str] = None) -> RestfulApiListResponse:
        """Returns a page of RestfulApis in a RestfulApiListResponse.

        This resource collection is paginated. Take a look at the examples to understand how to handle pagination.

        Args:
            cursor: Optional cursor to request the next page.

        Returns:
            RestfulApiListResponse containing one page of RESTful APIs on your account.

        Raises:
            pydantic.ValidationError: A malformed response was returned.
            requests.RequestException: A connection or an HTTP error occurred.
        """
        url = self._url_for("/restful_apis")

        query_parameters = {}
        if cursor:
            query_parameters["cursor"] = cursor

        response = requests.get(url, auth=self._auth, params=query_parameters)
        response.raise_for_status()
        return RestfulApiListResponse.parse_raw(response.text)

    @tenacity.retry(stop=tenacity.stop_after_attempt(5), after=tenacity.after_log(_logger, logging.INFO))
    def web_applications_get(self, id: UUID) -> WebApplicationGetResponse:
        """Returns a WebApplication in a WebApplicationGetResponse.

        Args:
            id: The UUID of the web application.

        Returns:
            WebApplicationGetResponse containing the web application of the supplied id.

        Raises:
            pydantic.ValidationError: A malformed response was returned.
            requests.RequestException: A connection or an HTTP error occurred.
        """
        url = self._url_for(f"/web_applications/{id}")
        response = requests.get(url, auth=self._auth)
        response.raise_for_status()
        return WebApplicationGetResponse.parse_raw(response.text)

    @tenacity.retry(stop=tenacity.stop_after_attempt(5), after=tenacity.after_log(_logger, logging.INFO))
    def web_applications_list(self, *, cursor: Optional[str] = None) -> WebApplicationListResponse:
        """Returns a page of WebApplications in a WebApplicationListResponse.

        This resource collection is paginated. Take a look at the examples to understand how to handle pagination.

        Args:
            cursor: Optional cursor to request the next page.

        Returns:
            WebApplicationListResponse containing one page of the web applications in your account.

        Raises:
            pydantic.ValidationError: A malformed response was returned.
            requests.RequestException: A connection or an HTTP error occurred.
        """
        url = self._url_for("/web_applications")

        query_parameters = {}
        if cursor:
            query_parameters["cursor"] = cursor

        response = requests.get(url, auth=self._auth, params=query_parameters)
        response.raise_for_status()
        return WebApplicationListResponse.parse_raw(response.text)

    @tenacity.retry(stop=tenacity.stop_after_attempt(5), after=tenacity.after_log(_logger, logging.INFO))
    def assets_list(
        self,
        *,
        cursor: Optional[str] = None,
        page_size: Optional[int] = 40,
        filter_by_asset_tags: Optional[Dict[str, Optional[List[str]]]] = None,
        filter_by_text: Optional[str] = None,
        filter_by_asset_types: Optional[List[AssetTypeEnum]] = None,
        filter_by_discovered_vias: Optional[List[DiscoveredViaEnum]] = None,
    ) -> AssetsListResponse:
        """Returns a page of WebApplications in a WebApplicationListResponse.

        This resource collection is paginated. Take a look at the examples to understand how to handle pagination.

        Args:
            cursor: Optional cursor to request the next page.
            page_size: Number of assets to include on one page
            filter_by_asset_tags: Assets must have asset tags
            filter_by_text: Assets title must have text in it
            filter_by_asset_type: Assets must be of certain type
            filter_by_discovered_via: Assets must be discovered via a specific way

        Returns:
            WebApplicationListResponse containing one page of the web applications in your account.

        Raises:
            pydantic.ValidationError: A malformed response was returned.
            requests.RequestException: A connection or an HTTP error occurred.
        """
        url = self._url_for("/assets", api_version="v2")

        query_parameters = {}
        if cursor:
            query_parameters["cursor"] = cursor
        if page_size:
            query_parameters["page_size"] = page_size  # type: ignore
        if filter_by_asset_tags:
            query_parameters["filter_by_asset_tags"] = json.dumps(filter_by_asset_tags)
        if filter_by_text:
            query_parameters["filter_by_text"] = filter_by_text
        if filter_by_asset_types:
            query_parameters["filter_by_asset_type"] = filter_by_asset_types  # type: ignore
        if filter_by_discovered_vias:
            query_parameters["filter_by_discovered_via"] = filter_by_discovered_vias  # type: ignore

        response = requests.get(url, auth=self._auth, params=query_parameters)
        response.raise_for_status()
        return AssetsListResponse.parse_raw(response.text)

    @tenacity.retry(stop=tenacity.stop_after_attempt(5), after=tenacity.after_log(_logger, logging.INFO))
    def openapi_definitions_list(self, *, cursor: Optional[str] = None) -> OpenApiDefinitionsResponse:
        """Returns a page of OpenApiDefinition in a OpenApiDefinitionsResponse.

        This resource collection is paginated. Take a look at the examples to understand how to handle pagination.

        Args:
            cursor: Optional cursor to request the next page.

        Returns:
            OpenApiDefinitionsResponse containing one page of the openapi definitions in your account.

        Raises:
            pydantic.ValidationError: A malformed response was returned.
            requests.RequestException: A connection or an HTTP error occurred.
        """
        url = self._url_for("/openapi_definitions")

        query_parameters = {}
        if cursor:
            query_parameters["cursor"] = cursor

        response = requests.get(url, auth=self._auth, params=query_parameters)
        response.raise_for_status()
        return OpenApiDefinitionsResponse.parse_raw(response.text)

    def openapi_definition_create(self, *, data: str) -> OpenApiDefinitionCreateResponse:
        """Returns a OpenApiDefinition in case of successful upload.

        Args:
            data: string containing unencoded OpenApi definition file

        Returns:
            OpenApiDefinition containing one definition uploaded to your account.

        Raises:
            pydantic.ValidationError: A malformed response was returned.
            requests.RequestException: A connection or an HTTP error occurred.
        """
        url = self._url_for("/openapi_definitions")

        response = requests.post(url, auth=self._auth, data=data.encode("utf-8"))
        response.raise_for_status()
        return OpenApiDefinitionCreateResponse.parse_raw(response.text)

    def cloud_authenticator_create(
        self, authenticator_request: CloudAuthenticatorCreateRequest
    ) -> CloudAuthenticatorCreateResponse:
        """Creates a new CloudAuthenticator for cloud discovery.

        Args:
            authenticator_request: a CloudAuthenticatorCreateRequest object

        Returns:
            CloudAuthenticatorCreateResponse containing the credential you sent to onboard your cloud environment.

        Raises:
            pydantic.ValidationError: A malformed response was returned.
            requests.RequestException: A connection or an HTTP error occurred.
        """
        url = self._url_for("/cloud_authenticators")

        response = requests.post(url, auth=self._auth, data=authenticator_request.json())
        response.raise_for_status()
        return CloudAuthenticatorCreateResponse.parse_raw(response.text)
