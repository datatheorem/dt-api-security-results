from enum import Enum
from typing import Any, Callable, Iterable, TypeVar

_PydanticEnumMixinT = TypeVar("_PydanticEnumMixinT", bound="PydanticEnumMixin")


class PydanticEnumMixin(Enum):
    @classmethod
    def __get_validators__(cls) -> Iterable[Callable]:
        """Used by pydantic"""
        yield cls.validate

    @classmethod
    def __get_validation_error__(cls) -> Exception:
        """Return expected values, used for error messages"""
        values = ", ".join(repr(attr) for attr in cls.__members__.keys())
        return ValueError(f"invalid value, expected one of {values}")

    @classmethod
    def validate(cls, v: Any) -> Any:
        """Input values are expected to be either members of the enum, integers or strings"""
        if isinstance(v, cls):
            return v
        elif isinstance(v, str) and v in cls.__members__:
            # Try Enum by name
            return cls.__members__[v]
        elif isinstance(v, (int, str)):
            # Fallback to value lookup
            value_lookup = {attr.value: attr for attr in cls.__members__.values()}
            try:
                return value_lookup[v]
            except KeyError:
                raise cls.__get_validation_error__()
        else:
            # Anything else is an error
            raise cls.__get_validation_error__()
