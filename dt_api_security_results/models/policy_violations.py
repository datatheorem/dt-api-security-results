from datetime import datetime
from typing import List, NewType, Optional
from uuid import UUID

from pydantic import BaseModel

# * API_SECURE = 1
# * WEB_SECURE = 2
# * CLOUD_SECURE = 3
# * MOBILE_SECURE = 4
ProductEnum = NewType("ProductEnum", str)


# * PROACTIVE
# * IMPORTANT
# * URGENT
RelevanceEnum = NewType("RelevanceEnum", str)


# * LOW
# * MEDIUM
# * HIGH
SeverityEnum = NewType("SeverityEnum", str)


# * WONT_FIX
# * POLICY_RULE_DELETED
# * AFFECTED_COMPONENT_DELETED
# * ASSET_GROUP_ATTACHED_POLICY_CHANGED
# * ASSET_GROUP_DELETED
# * ASSET_GROUP_MEMBERSHIP_DELETED
PolicyViolationExceptionEnum = NewType("PolicyViolationExceptionEnum", str)


# * OPEN
# * WONT_FIX
# * CLOSED
# * DELETED
PolicyViolationStatusEnum = NewType("PolicyViolationStatusEnum", str)


# * UNKNOWN
# * APIGEE
# * AMAZON_WEB_SERVICES
# * AZURE
# * GOOGLE_CLOUD_PLATFORM
# * MULESOFT
# * KONG
# * AXWAY
# * ON_PREMISE
CloudProviderEnum = NewType("CloudProviderEnum", str)


# * DOMAIN_NAME
# * NETWORK_SERVICE
# * RESTFUL_API
# * CLOUD_RESOURCE
# * WEB_APPLICATION
# * GRAPHQL_API
# * API_OPERATION
# * MOBILE_APPLICATION
# * MOBILE_SDK
# * GRPC_SERVICE
# * GRPC_METHOD
# * KUBERNETES_CLUSTER
# * KUBERNETES_CLUSTER_COMPONENT
# * SOAP_API_OPERATION
# * SOAP_API
AssetTypeEnum = NewType("AssetTypeEnum", str)


# * ENABLED
# * DELETED_BY_USER
PolicyRuleStatusEnum = NewType("PolicyRuleStatusEnum", str)


class CloudAccount(BaseModel):
    id: str
    cloud_account_type_name: str
    cloud_account_icon_url: str
    customer_supplied_name: Optional[str]


class AffectedAsset(BaseModel):
    id: UUID
    name: str
    url: Optional[str]
    hosted_on: CloudProviderEnum
    date_created: datetime

    asset_type: AssetTypeEnum
    asset_type_name: str
    asset_type_icon_url: str

    # Only set for cloud resources and Restful APIs discovered in a cloud account
    belongs_to_cloud_account: Optional[CloudAccount]
    aws_arn: Optional[str]
    cloud_console_url: Optional[str]


# * OWASP
# * PCI_DSS
# * NIST_800_53
# * FFIEC_VC2
# * CIS_BENCHMARK
ComplianceStandardEnum = NewType("ComplianceStandardEnum", str)


# There's too many to list. See:
# https://bitbucket.org/datatheorem/horizon-server/src/release/default_service/security_checks/compliance.py
ComplianceStandardCriteriaEnum = NewType("ComplianceStandardCriteriaEnum", str)


class ComplianceReference(BaseModel):
    compliance_standard: ComplianceStandardEnum
    compliance_standard_criteria: ComplianceStandardCriteriaEnum
    description: str
    link: str


class PolicyRuleType(BaseModel):
    id: UUID
    title: str
    description: str
    recommendation: str
    compliance_policy_references: List[ComplianceReference]


class PolicyRule(BaseModel):
    id: UUID
    status: PolicyRuleStatusEnum
    severity: SeverityEnum
    policy_rule_type: PolicyRuleType


class PolicyViolation(BaseModel):
    id: UUID
    status: PolicyViolationStatusEnum

    date_created: datetime
    date_resolved: Optional[datetime]
    date_deleted: Optional[datetime]

    exception_date_created: Optional[datetime]  # noqa: E701
    exception_type: Optional[PolicyViolationExceptionEnum]  # noqa: E701

    violated_policy_rule: PolicyRule

    affected_asset: AffectedAsset

    additional_info: Optional[str]


class _PolicyRuleType(BaseModel):
    id: UUID
    title: str
    description: str
    recommendation: str


class _PolicyRule(BaseModel):
    id: UUID
    status: PolicyRuleStatusEnum
    relevance: str
    policy_rule_type: _PolicyRuleType


class _AffectedAsset(BaseModel):
    id: UUID
    name: str
    url: Optional[str]
    date_created: datetime

    asset_type: AssetTypeEnum
    asset_type_name: str
    asset_type_icon_url: str

    # Only set for cloud resources and Restful APIs discovered in a cloud account
    belongs_to_cloud_account: Optional[CloudAccount]
    aws_arn: Optional[str]
    cloud_console_url: Optional[str]


class PolicyViolationAffectingAsset(BaseModel):
    id: UUID
    status: PolicyViolationStatusEnum

    date_created: datetime
    date_resolved: Optional[datetime]
    date_deleted: Optional[datetime]

    exception_date_created: Optional[datetime]  # noqa: E701
    exception_type: Optional[PolicyViolationExceptionEnum]  # noqa: E701

    violated_policy_rule: _PolicyRule

    affected_asset: _AffectedAsset

    additional_info: Optional[str]
