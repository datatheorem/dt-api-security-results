from datetime import datetime
from typing import Optional
from uuid import UUID

from pydantic import BaseConfig, BaseModel

from dt_api_security_results.models.enum_mixin import PydanticEnumMixin


class CloudAuthenticatorTypesEnum(PydanticEnumMixin):
    APIGEE = 1
    APIGEE_ON_PREM = 2
    AMAZON_WEB_SERVICES = 3
    AZURE = 4
    GOOGLE_CLOUD_PLATFORM = 5
    MULESOFT = 6
    KONG = 7
    AXWAY = 8


class ApigeeCredential(BaseModel):
    organization: str


class ApigeeOnPremCredential(BaseModel):
    organization: str
    management_api_url: str
    management_api_username: str
    management_api_password: str
    sso_login_url: Optional[str]


class AwsCredential(BaseModel):
    role_arn: str
    external_id: str


class AzureCredential(BaseModel):
    tenant_id: str
    client_id: str
    client_secret: str


class ServiceAccountCreateBody(BaseModel):
    auth_provider_x509_cert_url: str
    auth_uri: str
    client_email: str
    client_id: str
    client_x509_cert_url: str
    private_key: str
    private_key_id: str
    project_id: str
    token_uri: str
    type_: str

    class Config(BaseConfig):
        fields = {"type_": {"alias": "type"}}


class GcpCredential(BaseModel):
    organization_id: str
    service_account: ServiceAccountCreateBody


class KongCredential(BaseModel):
    kong_admin_subdomain: str
    kong_admin_token: str


class MulesoftCredential(BaseModel):
    organization_id: str
    account_username: str
    account_password: str


class AxwayCredential(BaseModel):
    client_id: str
    private_key: str
    organization_id: str


class CloudAuthenticatorStatusEnum(PydanticEnumMixin):
    ONLINE = 1
    OFFLINE = 2


class BaseAuthenticatorResponse(BaseModel):
    id: UUID
    added_by_actor_id: UUID
    added_by_actor_name: str
    added_by_user_id: UUID
    date_created: datetime
    added_by_external_user_email: Optional[str] = None


class ApigeeAuthenticatorResponse(BaseAuthenticatorResponse):
    organization: str


class ApigeeOnPremAuthenticatorResponse(BaseAuthenticatorResponse):
    organization: str
    management_api_url: str
    management_api_username: str


class AwsAuthenticatorResponse(BaseAuthenticatorResponse):
    role_arn: str
    external_id: str


class AzureAuthenticatorResponse(BaseAuthenticatorResponse):
    tenant_id: str
    client_id: str


class GcpAuthenticatorResponse(BaseAuthenticatorResponse):
    client_email: str
    organization_id: str
    private_key_id: str


class KongAuthenticatorResponse(BaseAuthenticatorResponse):
    kong_admin_subdomain: str


class MulesoftAuthenticatorResponse(BaseAuthenticatorResponse):
    organization_id: str
    account_username: str
    account_password: str


class AxwayAuthenticatorResponse(BaseAuthenticatorResponse):
    organization_id: str
    client_id: str
