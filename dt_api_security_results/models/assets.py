from datetime import datetime
from typing import List, NewType, Optional
from uuid import UUID

from pydantic import BaseModel, IPvAnyNetwork

# TODO: Hard-coded enums will break the client if we ever add new values to them server-side -- Adding to an enum that
# shows up in a response is a kind of API-breaking change.
#
# There are some alternatives:
#
# * Treat enums as strings. It is entirely the responsibility of the caller of the client code to handle new/previously
#   unknown values, and there is no way to statically type check these values-as-enums.
#
# * The API dynamically defines the valid enum values. When the client loads, it can fetch the definitions it needs.
#   Again, there is no way to statically type check these values-as-enums, since it has to be handled at runtime.
#
# * We continue to include enums in the client, and we update the possible values whenever we change it server side.
#   However, all previously deployed clients will break when they encounter the new enum values. The only advantage is
#   static type checking.
#
# For now, we're going with treating the enums as strings, and documenting example values. Perhaps in the future we can
# have a client dynamically fetch an enum's definition, whether from an OpenAPI document or from the API itself.


# The cloud provider where a particular resource is hosted. Currently defined values:
#
# * UNKNOWN
# * APIGEE
# * AMAZON_WEB_SERVICES
# * AZURE
# * GOOGLE_CLOUD_PLATFORM
# * MULESOFT
# * KONG
CloudProviderEnum = NewType("CloudProviderEnum", str)


class DomainName(BaseModel):
    """"""

    id: UUID
    name: str
    date_created: datetime


# The list of HTTP methods supported by OpenAPI 3.0.
#
# * GET
# * PUT
# * POST
# * DELETE
# * OPTIONS
# * HEAD
# * PATCH
# * TRACE
# * X_HORIZON_UNDEFINED_HTTP_METHOD
HttpMethodEnum = NewType("HttpMethodEnum", str)


# Whether we are able to connect to a Network Service, or not.
#
# * REACHABLE
# * NOT_REACHABLE
# * NOT_REACHABLE_CONNECTION_REJECTED
# * NOT_REACHABLE_CONNECTION_TIMED_OUT
# * NOT_REACHABLE_DNS_LOOKUP_FAILED
ConnectivityStatusEnum = NewType("ConnectivityStatusEnum", str)


#     AMAZON_WEB_SERVICES
#     MANUAL_IMPORT
#     MOBILE_APP_SCANS
#     APIGEE
#     CERTIFICATE_TRANSPARENCY
#     AZURE
#     GOOGLE_CLOUD_PLATFORM
#     REVERSE_IP_LOOKUP
#     WEB_APPLICATION_SCANS
#     MULESOFT
#     APIGEE_ON_PREM
#     SAN_EXTRACTION
#     RESTFUL_API_DISCOVERER
#     KONG
#     WEBSITE_CRAWLER
#     APPLICATION_FINGERPRINTING
#     AXWAY
#     SWAGGER_HUB
#     MOBILE_APP_STORE
#     MOBILE_PROTECT_USAGE
#     CLOUD_PROTECT
#     KUBERNETES_CLUSTER_ANALYSIS
DiscoveredViaEnum = NewType("DiscoveredViaEnum", str)


class ConnectivityScanResult(BaseModel):
    status: ConnectivityStatusEnum
    date_created: datetime


class NetworkService(BaseModel):
    id: UUID
    date_created: datetime
    domain_name_id: UUID
    port: int
    application_layer_protocol: str

    is_tls_encrypted: bool
    url: str
    discovered_via: DiscoveredViaEnum
    hosted_on: Optional[CloudProviderEnum]

    connectivity_scan_results: List[ConnectivityScanResult]


class ApiOperation(BaseModel):
    id: UUID
    date_created: datetime
    restful_api_id: UUID

    path: str
    http_method: HttpMethodEnum
    is_authenticated: bool


class RestfulApi(BaseModel):
    id: UUID
    date_created: datetime

    network_service_id: UUID
    title: str
    base_url: str
    base_path: str
    api_operations: List[ApiOperation]


class WebApplication(BaseModel):
    id: UUID
    date_created: datetime
    discovered_via: DiscoveredViaEnum
    base_url: str
    base_path: str
    network_service_id: UUID


class CloudResource(BaseModel):
    id: UUID
    date_created: datetime
    discovered_via: DiscoveredViaEnum

    name: str
    cloud_console_url: str
    asset_type_name: str
    url: Optional[str]
    region: Optional[str]
    runtime: Optional[str]
    aws_arn: Optional[str]
    gcp_project_id: Optional[str]
    mulesoft_asset_id: Optional[str]
    azure_tenant_id: Optional[str]


class OpenApiDefinition(BaseModel):
    id: UUID
    title: str
    original_document: str
    status: str

    discovered_via: str
    discovered_via_apigee_authenticator_id: Optional[UUID]
    discovered_via_apigee_on_prem_authenticator_id: Optional[UUID]
    discovered_via_aws_authenticator_id: Optional[UUID]
    discovered_via_azure_authenticator_id: Optional[UUID]
    discovered_via_gcp_authenticator_id: Optional[UUID]
    discovered_via_mobile_application_id: Optional[UUID]
    discovered_via_mulesoft_authenticator_id: Optional[UUID]
    discovered_via_kong_authenticator_id: Optional[UUID]
    discovered_via_axway_authenticator_id: Optional[UUID]
    discovered_via_user_id: Optional[UUID]
    date_created: datetime

    class Config:
        max_anystr_length = None


class GraphqlApi(BaseModel):
    id: UUID
    url: str
    aws_arn: Optional[str]


class IpRange(BaseModel):
    id: UUID
    ip_range: IPvAnyNetwork
    has_continuous_discovery_enabled: bool
