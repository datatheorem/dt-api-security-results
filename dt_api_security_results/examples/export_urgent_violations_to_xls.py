from argparse import ArgumentParser
from dataclasses import dataclass
from datetime import datetime
from typing import List, Optional
from uuid import UUID

import xlwt

from dt_api_security_results.client import ApiSecurityResultsClient
from dt_api_security_results.models.policy_violations import SeverityEnum


@dataclass
class ExportedViolationRow:
    asset_url: Optional[str]
    asset_aws_arn: Optional[str]
    violation_uuid: UUID
    title: str
    severity: SeverityEnum
    affected_asset_name: str
    affected_asset_type: str
    affected_asset_cloud_console_url: Optional[str]
    description: str
    additional_information: Optional[str]
    recommendation: str
    date_discovered: datetime

    @property
    def violation_portal_url(self) -> str:
        return f"https://www.securetheorem.com/api/inspect/policy-violations/{self.violation_uuid}"


def retrieve_high_severity_violations(api_key: str) -> List[ExportedViolationRow]:  # noqa: C901 (complexity)
    # Retrieve all of the open violations that are HIGH severity
    client = ApiSecurityResultsClient(api_key=api_key)
    print("Fetching all HIGH severity violations")
    cursor = None

    # We have to go through all the pages
    all_urgent_violations = []
    while True:
        response = client.policy_violations_list(cursor=cursor, filter_by_severity=[SeverityEnum("HIGH")])
        print(f"Fetched {len(response.policy_violations)} violations")

        # Make the data easy to query
        all_urgent_violations.extend(response.policy_violations)

        cursor = response.pagination_information.next_cursor
        if not cursor:
            # We went through all the pages; all done here
            break

    # Start processing each violation
    exported_violations = []
    for violation in all_urgent_violations:
        # Skip violations with an exception (ie. manually closed by a user in the portal)
        if violation.exception_type:
            continue

        affected_asset = violation.affected_asset

        # Lookup meta-data about the policy violation
        policy_rule = violation.violated_policy_rule
        policy_rule_type = policy_rule.policy_rule_type

        exported_violations.append(
            ExportedViolationRow(
                asset_url=affected_asset.url,
                asset_aws_arn=affected_asset.aws_arn,
                violation_uuid=violation.id,
                title=policy_rule_type.title,
                severity=policy_rule.severity,
                affected_asset_name=affected_asset.name,
                affected_asset_type=affected_asset.asset_type,
                affected_asset_cloud_console_url=affected_asset.cloud_console_url,
                description=policy_rule_type.description,
                additional_information=violation.additional_info,
                recommendation=policy_rule_type.recommendation,
                date_discovered=violation.date_created,
            )
        )
    return exported_violations


def make_argument_parser() -> ArgumentParser:
    parser = ArgumentParser()
    parser.add_argument("api_key")
    return parser


def main() -> None:
    args = make_argument_parser().parse_args()
    xls_file = "urgent_violations.xls"

    exported_violations = retrieve_high_severity_violations(args.api_key)

    print(f"Dumping {len(exported_violations)} violations from to {xls_file}...")
    book = xlwt.Workbook()
    sheet = book.add_sheet("Policy Violations")

    sheet.write(0, 0, "Unique ID")
    sheet.write(0, 1, "Title")
    sheet.write(0, 2, "Severity")
    sheet.write(0, 3, "Asset Name")
    sheet.write(0, 4, "Asset Type")
    sheet.write(0, 5, "Asset Cloud Console URL")
    sheet.write(0, 6, "Description")
    sheet.write(0, 7, "Additional Information")
    sheet.write(0, 8, "Recommendation")
    sheet.write(0, 9, "Date Discovered")
    sheet.write(0, 10, "Data Theorem Portal URL")
    sheet.write(0, 11, "Asset URL")
    sheet.write(0, 12, "Asset AWS ARN")

    for index, violation_row in enumerate(exported_violations, start=1):
        # Additional info can be longer then the maximum allowed by Excel
        if violation_row.additional_information and len(violation_row.additional_information) < 32000:
            sanitized_additional_info = violation_row.additional_information
        else:
            sanitized_additional_info = ""

        sheet.write(index, 0, str(violation_row.violation_uuid))
        sheet.write(index, 1, violation_row.title)
        sheet.write(index, 2, violation_row.severity)
        sheet.write(index, 3, violation_row.affected_asset_name)
        sheet.write(index, 4, violation_row.affected_asset_type)
        sheet.write(index, 5, violation_row.affected_asset_cloud_console_url)
        sheet.write(index, 6, violation_row.description)
        sheet.write(index, 7, sanitized_additional_info)
        sheet.write(index, 8, violation_row.recommendation)
        sheet.write(index, 9, violation_row.date_discovered.date().isoformat())
        sheet.write(index, 10, violation_row.violation_portal_url)
        sheet.write(index, 11, violation_row.asset_url)
        sheet.write(index, 12, violation_row.asset_aws_arn)

    book.save(xls_file)
    print("All done!")


if __name__ == "__main__":
    main()
