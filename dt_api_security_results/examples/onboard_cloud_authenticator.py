""" Example script showing how to onboard a cloud authenticator.
"""
import logging

from dt_api_security_results.client import ApiSecurityResultsClient, CloudAuthenticatorCreateRequest
from dt_api_security_results.models.cloud_authenticators import AwsCredential, CloudAuthenticatorTypesEnum

API_KEY = "REPLACE WITH YOUR API KEY"
ROLE_ARN = "REPLACE WITH YOUR ROLE ARN"
EXTERNAL_ID = "REPLACE WITH YOUR EXTERNAL ID"

logging.basicConfig(level=logging.INFO, format="%(asctime)s %(levelname)s %(message)s")

if __name__ == "__main__":
    client = ApiSecurityResultsClient(api_key=API_KEY)

    request = CloudAuthenticatorCreateRequest(
        cloud_authenticator_type=CloudAuthenticatorTypesEnum.AMAZON_WEB_SERVICES,
        aws_credential=AwsCredential(
            role_arn=ROLE_ARN,
            external_id=EXTERNAL_ID,
        ),
    )
    try:
        response = client.cloud_authenticator_create(authenticator_request=request)
    except Exception:
        logging.exception("An error occurred.")
    else:
        logging.info(f"Successfully onboarded cloud authenticator: {response.json()}")
    logging.info("All done.")
