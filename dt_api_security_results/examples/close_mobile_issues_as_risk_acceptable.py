""" Script to close mobile issues from files like "DT findings - Risk acceptable.xlsx"

usage: close_mobile_issues_as_risk_acceptable.py [-h] --xlsx XLSX

One input file type expected

arguments:
  --xlsx XLSX  An XLSX file to parse mobile issues from

optional arguments:
  -h, --help   show this help message and exit
  --key KEY    API Key (optional, if not provided in the environment)

"""

import argparse
import os
from asyncio.log import logger
from pathlib import Path
from typing import List

import pydantic
import requests

POST_URL = "https://api.securetheorem.com/apis/mobile_security/results/v2/security_findings/{security_finding_id}/close"

# for testing before calling close
GET_URL = "https://api.securetheorem.com/apis/mobile_security/results/v2/security_findings/{security_finding_id}"

# override as you like
CLOSING_REASON = "Issue requested to be closed"


class MobileIssue(pydantic.BaseModel):
    title: str
    app_name: str
    platform: str
    severity: str
    issue_id: int
    mobile_app_id: int
    security_finding_id: int
    url: str


_EXPECTED_HEADERS = (
    "Issue Title",
    "App Name",
    "Platform",
    "Severity",
    "Issue ID",
    "Mobile App ID",
    "Security Finding ID",
    "Portal URL",
)


def parse_spreadsheet(spreadsheet_path: Path) -> List[MobileIssue]:
    from openpyxl import load_workbook

    workbook = load_workbook(spreadsheet_path)
    worksheet = workbook.active
    rows = list(worksheet.values)
    if rows[0] != _EXPECTED_HEADERS:
        raise ValueError("Unexpected XLSX file format, column headers not respected")

    mobile_issues = []
    for i, row in enumerate(rows[1:]):  # skip first row of headers
        row_id = i + 1
        try:
            mobile_issues.append(
                MobileIssue(
                    title=row[0],
                    app_name=row[1],
                    platform=row[2],
                    severity=row[3],
                    issue_id=row[4],
                    mobile_app_id=row[5],
                    security_finding_id=row[6],
                    url=row[7],
                )
            )
        except Exception:
            logger.error(f"Unable to parse row {row_id}")

    return mobile_issues


# check if the file exists; raise exception if it doesn't
def check_file_exists(file_path: str) -> None:
    if not os.path.exists(file_path):
        raise ValueError(f"Error: Input file ({file_path}) does not exist")


def compile_list(args: argparse.Namespace) -> List[MobileIssue]:
    if not args.xlsx:
        print("Expected input file missing: i.e. --xlsx $file")
        exit(1)

    check_file_exists(args.xlsx)
    return parse_spreadsheet(Path(args.xlsx))


def format_security_finding_id(security_finding_id: int) -> str:
    # Pad zeros on the left so "1234" becomes "001234"
    return str(security_finding_id).zfill(6)


class FakeResponse(pydantic.BaseModel):
    # Helpful if you don't want to call requests.post for testing
    status_code: int = 200


def close_security_finding_by_id(security_finding_id: int) -> requests.Response:
    print(f"closing {security_finding_id}...")
    response = requests.post(
        POST_URL.format(security_finding_id=format_security_finding_id(security_finding_id)),
        headers={"Authorization": f"APIKey {DT_API_KEY}"},
        json={
            "aggregated_status": "CLOSED_RISK_ACCEPTED",
            "is_permanently_closed": True,
            "reason": CLOSING_REASON,
        },
    )
    return response


def print_results(success: set, failure: set) -> None:
    print("\n")
    print("All done! Results:\n***")
    print(f"Successfully closed {len(success)} mobile issues:")
    for security_finding_id in success:
        print(security_finding_id)
    print(f"\nFailed to close {len(failure)} mobile issues:")
    for security_finding_id in failure:
        print(security_finding_id)
    print("***")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--xlsx", help="An XLSX file to parse")
    parser.add_argument("--key", help="API Key (optional, if not provided in the environment)")
    args = parser.parse_args()

    # Check if the API key is supplied from command line or the DT_API_KEY environment variable
    DT_API_KEY = args.key
    if not DT_API_KEY:
        DT_API_KEY = os.environ.get("DT_API_KEY")

    if not DT_API_KEY:
        print("Please export DT_API_KEY or supply it in the --key parameter")
        exit(-1)

    mobile_issues = compile_list(args)
    security_finding_ids = list(set([mobile_issue.security_finding_id for mobile_issue in mobile_issues]))

    # Get confirmation to proceed
    answer = input(
        f"About to close the following {len(security_finding_ids)} mobile issue security "
        f"finding ids:\n\n{security_finding_ids}\n\nContinue? y/n\n"
    )
    successful_list = set()
    failed_list = set()
    if answer == "y":
        response = requests.get(
            GET_URL.format(security_finding_id=format_security_finding_id(security_finding_ids[0])),
            headers={"Authorization": f"APIKey {DT_API_KEY}"},
        )
        if response.status_code != 200:
            raise ValueError("Test get request fails with: " + response.text)

        # iterate over security finding ids
        for security_finding_id in security_finding_ids:
            response = close_security_finding_by_id(security_finding_id)
            if response.status_code == 200:
                # success - add to the list of successfully processed issues
                logger.info(f"Successfully closed: {security_finding_id}")
                successful_list.add(f"{security_finding_id},{response.status_code}")
            else:
                # failed for some reason; add to the list of failed issues
                logger.error(f'Unable to close {security_finding_id}: "{response.text}"')
                failed_list.add(f'{security_finding_id},{response.status_code},"{response.text}"')

    print_results(successful_list, failed_list)
