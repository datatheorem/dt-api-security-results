""" Example script showing how to upload IP ranges from a provided text file

usage: upload_definitions.py [-h] file

positional arguments:
  file         File containing the IP ranges to upload

optional arguments:
  -h, --help  show this help message and exit
"""
import argparse
import logging

from pydantic import IPvAnyNetwork

from dt_api_security_results.client import ApiSecurityResultsClient

API_KEY = "REPLACE WITH YOUR API KEY"

logging.basicConfig(level=logging.INFO, format="%(asctime)s %(levelname)s %(message)s")
parser = argparse.ArgumentParser()
parser.add_argument("file", help="File containing the IP ranges to upload")

if __name__ == "__main__":
    args = parser.parse_args()

    with open(args.file, "r") as f:
        # The IP range format must be like 1.1.1.0/24
        all_ip_networks = [IPvAnyNetwork.validate(line) for line in f.read().split("\n")]

    client = ApiSecurityResultsClient(api_key=API_KEY)
    for ip_network in all_ip_networks:
        logging.info(f"Sending {ip_network}...")
        resp = client.ip_ranges_create(ip_network)
        logging.info(f"IP range {ip_network} added - {resp.json()}")

    logging.info("All done.")
