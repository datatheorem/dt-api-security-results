""" Script to add multiple domains with various input file formats

usage: onboard_domain_names.py [-h] [--csv CSV] [--txt TXT] [--xlsx XLSX]

One input file type expected

optional arguments:
  -h, --help   show this help message and exit
  --key KEY    API Key (optional, if not provided in the environment)
  --csv CSV    A CSV file to parse domain names from
  --txt TXT    A TXT file to parse domain names from
  --xlsx XLSX  An XLSX file to parse domain names from
"""

import argparse
import ipaddress
import os
from asyncio.log import logger
from pathlib import Path
from typing import Set, Tuple

import requests

POST_URL = "https://api.securetheorem.com/apis/api_security/results/v2/domain_names"


class InvalidDomainError(Exception):
    ...


# parse a string, e.g., a row from the input file
# return domain string OR raise an exception if domain cannot be processed
def parse_domain(domain_or_ip_address: str) -> str:
    try:
        ipaddress.ip_address(domain_or_ip_address)
        # ignore IP addresses
        raise InvalidDomainError("Invalid domain: IP address detected ({domain_or_ip_address})")
    except ValueError:
        # The cell contains a domain name - keep it for now
        domain = domain_or_ip_address

    if not domain:
        raise InvalidDomainError("Invalid domain: empty string provided")
    # Remove empty cells
    if "(empty)" in domain:
        raise InvalidDomainError(f"Invalid domain: '(empty)' string detected ({domain})")
    # Localhost cannot be onboarded
    if "localhost" in domain:
        raise InvalidDomainError(f"Invalid domain: 'localhost' string detected ({domain})")
    # Remove domains internal to AWS; may need to exend this list
    if "compute.internal" in domain:
        raise InvalidDomainError(f"Invalid domain: 'compute.internal' string detected ({domain})")
    # Remove port number
    if ":" in domain:
        return domain.split(":")[0]
    return domain


# Parse XLS file and read domains contained therein
# Return domains that can (potentially) be onboarded and a list of domains that cannot be, e.g., localhost
def parse_spreadsheet_with_domains(spreadsheet_path: Path) -> Tuple[Set[str], Set[str]]:
    from openpyxl import load_workbook

    # Extract the list of domains from the spreadsheet
    workbook = load_workbook(spreadsheet_path)
    worksheet = workbook.active

    all_domains_that_can_be_onboarded = set()
    all_domains_that_cannot_be_onboarded = set()

    for row_content in worksheet.values:
        try:
            domain = parse_domain(row_content[0])
            all_domains_that_can_be_onboarded.add(domain)
        except InvalidDomainError:
            logger.error(f"Unable to onboard domain: {str(InvalidDomainError)}")
            all_domains_that_cannot_be_onboarded.add(f"{domain}: {str(InvalidDomainError)}")
            pass

    return all_domains_that_can_be_onboarded, all_domains_that_cannot_be_onboarded


# Parse CSV or TXT file and read domains contained therein
# Return domains that can (potentially) be onboarded and a list of domains that cannot be, e.g., localhost
def parse_csv_or_txt_with_domains(file_path: Path) -> Tuple[Set[str], Set[str]]:
    all_domains_that_can_be_onboarded = set()
    all_domains_that_cannot_be_onboarded = set()
    with open(file_path) as file:
        for line in file.readlines():
            try:
                domain = parse_domain(line.rstrip())
                all_domains_that_can_be_onboarded.add(domain)
            except InvalidDomainError:
                logger.error(f"Unable to onboard domain: {str(InvalidDomainError)}")
                all_domains_that_cannot_be_onboarded.add(f"{domain}: {str(InvalidDomainError)}")
                pass

    return all_domains_that_can_be_onboarded, all_domains_that_cannot_be_onboarded


# Onboard a single domain to DT
# Return HTTP resonse to the caller for analysis
def onboard_domain_to_dt(domain_name: str) -> requests.Response:
    headers = {"Authorization": f"APIKey {DT_API_KEY}"}
    print(f"Adding {domain_name}...")
    response = requests.post(
        POST_URL,
        headers=headers,
        json={"domain_name": domain_name},
    )
    return response


# check if the file exists; raise exception if it doesn't
def check_file_exists(file_path: str) -> None:
    if not os.path.exists(file_path):
        raise ValueError(f"Error: Input file ({file_path}) does not exist")


# Compile domains according to supplied parameters
def compile_domain_list(args: argparse.Namespace) -> Tuple[Set[str], Set[str]]:
    # Functions return a set of valid domains to be onboarded and a set of invalid entries, e.g., localhost
    if args.csv:
        check_file_exists(args.csv)
        valid_domains, invalid_domains = parse_csv_or_txt_with_domains(Path(args.csv))
    elif args.txt:
        check_file_exists(args.txt)
        valid_domains, invalid_domains = parse_csv_or_txt_with_domains(Path(args.txt))
    elif args.xlsx:
        check_file_exists(args.xlsx)
        valid_domains, invalid_domains = parse_spreadsheet_with_domains(Path(args.xlsx))
    else:
        parser.print_help()
        exit(-1)

    return valid_domains, invalid_domains


def print_results(success: set, failure: set) -> None:
    # print the results
    print("\n")
    print("All done! Results:\n***")
    print(f"Successfully onboarded {len(success)} domains:")
    for domain in success:
        print(domain)
    print(f"\nFailed to onboard {len(failure)} domains:")
    for domain in failure:
        print(domain)
    print("***")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--csv", help="A CSV file to parse domain names from")
    parser.add_argument("--txt", help="A TXT file to parse domain names from")
    parser.add_argument("--xlsx", help="An XLSX file to parse domain names from")
    parser.add_argument("--key", help="API Key (optional, if not provided in the environment)")
    args = parser.parse_args()

    # Check if the API key is supplied from command line or the DT_API_KEY environment variable
    DT_API_KEY = args.key
    if not DT_API_KEY:
        DT_API_KEY = os.environ.get("DT_API_KEY")

    if not DT_API_KEY:
        print("Please export DT_API_KEY or supply it in the --key parameter")
        exit(-1)

    successful_domains = set()
    all_domains, failed_domains = compile_domain_list(args)

    # Get confirmation to proceed
    answer = input(f"About to onboard the following {len(all_domains)} domains:\n\n{all_domains}\n\nContinue? y/n\n")
    if answer == "y":
        # iterate over domains
        for domain in all_domains:
            response = onboard_domain_to_dt(domain)
            if response.status_code == 201:
                # success - add to the list of successfully processed domains
                logger.info(f"Successfully onboarded domain: {domain}")
                successful_domains.add(f"{domain},{response.status_code}")
            else:
                # failed for some reason; add to the list of failed domains
                logger.error(f'Unable to onboard domain {domain}: "{response.text}"')
                failed_domains.add(f'{domain},{response.status_code},"{response.text}"')

    print_results(successful_domains, failed_domains)
