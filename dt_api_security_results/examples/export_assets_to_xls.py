import json
import logging
import os
from argparse import ArgumentParser
from dataclasses import dataclass
from datetime import datetime
from typing import Dict, List, Optional
from uuid import UUID

import xlwt

from dt_api_security_results.client import ApiSecurityResultsClient, AssetGroupField, AssetTagAppliedToAssetField
from dt_api_security_results.models.assets import ApiOperation, DiscoveredViaEnum
from dt_api_security_results.models.policy_violations import AssetTypeEnum, PolicyViolationAffectingAsset, SeverityEnum

logging.basicConfig(level=logging.INFO)


@dataclass
class ExportedViolationRow:
    asset_url: Optional[str]
    asset_aws_arn: Optional[str]
    violation_uuid: UUID
    title: str
    severity: SeverityEnum
    affected_asset_name: str
    affected_asset_type: str
    affected_asset_cloud_console_url: Optional[str]
    description: str
    additional_information: Optional[str]
    recommendation: str
    date_discovered: datetime

    @property
    def violation_portal_url(self) -> str:
        return f"https://www.securetheorem.com/api/inspect/policy-violations/{self.violation_uuid}"


@dataclass
class ExportedAssetRow:
    asset_uuid: UUID
    name: str
    url: Optional[str]
    asset_type: str
    asset_type_icon_url: str
    asset_type_name: str
    belongs_to_asset_group: Optional[AssetGroupField]

    open_urgent_policy_violations_count: int
    open_important_policy_violations_count: int
    open_proactive_policy_violations_count: int

    discovered_via: str
    discovered_via_icon_url: str
    discovered_via_name: str

    hosted_on: str
    hosted_on_icon_url: str
    hosted_on_name: str

    status: str
    date_created: datetime
    date_no_longer_accessible: Optional[datetime]

    tags: List[AssetTagAppliedToAssetField]

    violations: List[PolicyViolationAffectingAsset]

    api_operations: Optional[List[ApiOperation]]

    @property
    def asset_portal_url(self) -> str:
        return f"https://www.securetheorem.com/api/inspect/assets/{self.asset_uuid}"


def fetch_all_violations_affecting_asset(
    client: ApiSecurityResultsClient, asset_id: UUID, cursor: Optional[str]
) -> List[PolicyViolationAffectingAsset]:
    all_violations = []
    fetched_so_far = 0
    while True:
        violation_response = client.policy_violations_affecting_asset_list(id=asset_id, cursor=cursor)
        fetched_so_far += len(violation_response.policy_violations)
        total_number = violation_response.pagination_information.total_count
        print(
            f"Fetched {fetched_so_far} out of {total_number} policy violations ({total_number - fetched_so_far} left)"
        )

        # Make the data easy to query
        all_violations.extend(violation_response.policy_violations)

        cursor = violation_response.pagination_information.next_cursor
        if not cursor:
            # We went through all the pages; all done here
            break

    return all_violations


def fetch_all_api_operations_of_restful_api(
    client: ApiSecurityResultsClient, restful_api_id: UUID
) -> List[ApiOperation]:
    restful_api_response = client.restful_apis_get(id=restful_api_id)
    print("Fetched api operations")

    return restful_api_response.restful_apis[0].api_operations


def write_to_xslx(sheet: xlwt.Worksheet, assets: list, index: int) -> int:
    MAX_CELL_SIZE: int = 32000

    for asset_row in assets:
        violations_combined_info = []
        for v in asset_row.violations:
            violations_combined_info.append(
                {
                    "id": str(v.id),
                    "title": v.violated_policy_rule.policy_rule_type.title,
                    "severity": v.violated_policy_rule.relevance,
                }
            )
        all_violations: str = "\n".join({v.violated_policy_rule.policy_rule_type.title for v in asset_row.violations})
        all_violations = (
            (all_violations[:MAX_CELL_SIZE] + "...") if len(all_violations) > MAX_CELL_SIZE else all_violations
        )

        violations_combined: str = json.dumps(violations_combined_info)
        violations_combined = (
            (violations_combined[:MAX_CELL_SIZE] + "...")
            if len(violations_combined) > MAX_CELL_SIZE
            else violations_combined
        )

        api_ops: str = (
            "\n".join(f"{op.http_method} at {op.path}" for op in asset_row.api_operations)
            if asset_row.api_operations
            else ""
        )
        api_ops = (api_ops[:MAX_CELL_SIZE] + "...") if len(api_ops) > MAX_CELL_SIZE else api_ops

        sheet.write(index, 0, str(asset_row.asset_uuid))
        sheet.write(index, 1, str(asset_row.asset_type))
        sheet.write(index, 2, str(asset_row.name))
        sheet.write(index, 3, str(asset_row.url))
        sheet.write(index, 4, str(asset_row.open_important_policy_violations_count))
        sheet.write(index, 5, str(asset_row.open_proactive_policy_violations_count))
        sheet.write(index, 6, str(asset_row.open_urgent_policy_violations_count))
        sheet.write(index, 7, str(asset_row.hosted_on))
        sheet.write(index, 8, str(asset_row.discovered_via_name))
        sheet.write(index, 9, str(asset_row.status))
        sheet.write(index, 10, str(asset_row.date_created))
        sheet.write(index, 11, str({o.tag: o.value for o in asset_row.tags}))
        sheet.write(index, 12, all_violations)
        sheet.write(index, 13, violations_combined)
        sheet.write(index, 14, api_ops)

        index += 1

    return index


def retrieve_assets(
    api_key: str,
    sheet: xlwt.Worksheet,
    filter_by_asset_tags: Optional[Dict[str, Optional[List[str]]]] = None,
    filter_by_text: Optional[str] = None,
    filter_by_asset_types: Optional[List[AssetTypeEnum]] = None,
    filter_by_discovered_vias: Optional[List[DiscoveredViaEnum]] = None,
) -> int:  # noqa: C901 (complexity)
    client = ApiSecurityResultsClient(api_key=api_key)
    print("Fetching all assets")
    cursor = None

    # We have to go through all the pages
    fetched_so_far = 0
    index = 1
    while True:
        response = client.assets_list(
            cursor=cursor,
            filter_by_asset_tags=filter_by_asset_tags,
            filter_by_text=filter_by_text,
            filter_by_asset_types=filter_by_asset_types,
            filter_by_discovered_vias=filter_by_discovered_vias,
        )
        total_number = response.pagination_information.total_count
        fetched_so_far += len(response.assets)
        print(f"Fetched {fetched_so_far} out of {total_number} assets ({total_number - fetched_so_far} left)")

        if len(response.assets) > 0:
            exported_assets = []
            for asset in response.assets:
                violations = fetch_all_violations_affecting_asset(client=client, asset_id=asset.id, cursor=cursor)
                if asset.asset_type == "RESTFUL_API":
                    api_operations = fetch_all_api_operations_of_restful_api(client=client, restful_api_id=asset.id)
                else:
                    api_operations = None

                exported_assets.append(
                    ExportedAssetRow(
                        asset_uuid=asset.id,
                        name=asset.name,
                        url=asset.url,
                        asset_type=asset.asset_type,
                        asset_type_icon_url=asset.asset_type_icon_url,
                        asset_type_name=asset.asset_type_name,
                        belongs_to_asset_group=asset.belongs_to_asset_group,
                        open_urgent_policy_violations_count=asset.open_urgent_policy_violations_count,
                        open_important_policy_violations_count=asset.open_important_policy_violations_count,
                        open_proactive_policy_violations_count=asset.open_proactive_policy_violations_count,
                        discovered_via=asset.discovered_via,
                        discovered_via_icon_url=asset.discovered_via_icon_url,
                        discovered_via_name=asset.discovered_via_name,
                        hosted_on=asset.hosted_on,
                        hosted_on_icon_url=asset.hosted_on_icon_url,
                        hosted_on_name=asset.hosted_on_name,
                        status=asset.status,
                        date_created=asset.date_created,
                        date_no_longer_accessible=asset.date_no_longer_accessible,
                        tags=asset.tags,
                        violations=violations,
                        api_operations=api_operations,
                    )
                )

            print(f"Index prior to write: {index}")
            index = write_to_xslx(sheet, exported_assets, index)
            print(f"Index after write: {index}")

        cursor = response.pagination_information.next_cursor
        if not cursor:
            # We went through all the pages; all done here
            break

    return fetched_so_far


def make_argument_parser() -> ArgumentParser:
    parser = ArgumentParser()
    parser.add_argument(
        "--key",
        default=os.environ.get("DT_API_KEY"),
        type=str,
        help="API Key (optional, if not provided in the DT_API_KEY environment variable)",
    )
    parser.add_argument("dest_file", type=str, help="An XLSX file to save the assets to")
    return parser


def main() -> None:
    args = make_argument_parser().parse_args()
    xls_file = args.dest_file

    # Check if the API key is supplied from command line or the DT_API_KEY environment variable
    DT_API_KEY = args.key
    if not DT_API_KEY:
        print("Please export DT_API_KEY or supply it in the --key parameter")
        exit(-1)

    print(f"Writing assets to {xls_file}...")

    book = xlwt.Workbook()
    sheet = book.add_sheet("Assets")

    sheet.write(0, 0, "Unique ID")
    sheet.write(0, 1, "Asset Type")
    sheet.write(0, 2, "Asset Name")
    sheet.write(0, 3, "Asset Url")
    sheet.write(0, 4, "Open Important Policy Violations Count")
    sheet.write(0, 5, "Open Proactive Policy Violations Count")
    sheet.write(0, 6, "Open Urgent Policy Violations Count")
    sheet.write(0, 7, "Hosted On")
    sheet.write(0, 8, "Discovered Via")
    sheet.write(0, 9, "Status")
    sheet.write(0, 10, "Discovery Date")
    sheet.write(0, 11, "Asset Tags")
    sheet.write(0, 12, "Policy Violation Titles (combined)")
    sheet.write(0, 13, "Policy Violation info (json)")
    sheet.write(0, 14, "API Operations (if applicable)")

    # MODIFY FILTERS HERE
    total_assets = retrieve_assets(
        DT_API_KEY,
        sheet,
        # filter_by_asset_tags={"servers": None},
        filter_by_asset_types=[AssetTypeEnum("RESTFUL_API")],  # uncomment to only show restful apis
        filter_by_text="Swagger",  # uncomment to filter by keywords
        # note: see other accepted string enum values in the comment section where DiscoveredViaEnum is defined
        # filter_by_discovered_vias=[DiscoveredViaEnum("WEBSITE_CRAWLER"), DiscoveredViaEnum("MANUAL_IMPORT")],
    )

    book.save(xls_file)
    print(f"All done! {total_assets} assets written out to {xls_file}.")


if __name__ == "__main__":
    main()
