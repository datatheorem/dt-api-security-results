""" Example script showing how to upload all OpenApi definitions from a provided directory

usage: upload_definitions.py [-h] --key DT_API_KEY --dir dir

positional arguments:
  --dir DIR   Directory containing OpenAPI definition files

optional arguments:
  --key KEY   API Key (optional, if not provided in the DT_API_KEY environment environment)
  --help -h   show this help message and exit

"""
import argparse
import logging
import os

import requests

from dt_api_security_results.client import ApiSecurityResultsClient

logging.basicConfig(level=logging.INFO, format="%(asctime)s %(levelname)s %(message)s")
parser = argparse.ArgumentParser()
parser.add_argument("--dir", help="Directory containing OpenAPI definition files")
parser.add_argument("--key", help="API Key (optional, if not provided in the environment)")


def print_results(success: set, failure: set) -> None:
    # print the results
    print("\n")
    print("All done! Results:\n***")
    print(f"Successfully onboarded {len(success)} definitions:")
    for definition in success:
        print(definition)
    print(f"\nFailed to onboard {len(failure)} definitions:")
    for definition in failure:
        print(definition)
    print("***")


if __name__ == "__main__":
    args = parser.parse_args()

    # Check if the API key is supplied from command line or the DT_API_KEY environment variable
    DT_API_KEY = args.key
    if not DT_API_KEY:
        DT_API_KEY = os.environ.get("DT_API_KEY")

    if not DT_API_KEY:
        print("Please export DT_API_KEY or supply it in the --key parameter")
        exit(-1)

    successful_files = set()
    failed_files = set()

    client = ApiSecurityResultsClient(api_key=DT_API_KEY)
    for root, dirs, files in os.walk(args.dir):
        for name in files:
            if not (name.endswith(".json") or name.endswith(".yaml")):
                # skip *.txt and other files
                continue
            filename = os.path.join(root, name)
            logging.info(f"Uploading {filename}...")
            with open(filename, "r") as f:
                try:
                    result = client.openapi_definition_create(data=f.read())
                    logging.info("Completed")
                    successful_files.add(f'{filename},"{result}"')
                except requests.RequestException as e:
                    logging.info("Failed")
                    failed_files.add(f'{filename},"{e}"')

    print_results(successful_files, failed_files)
