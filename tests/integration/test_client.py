import pytest
from requests import HTTPError

import dt_api_security_results.client
from dt_api_security_results.client import CloudAuthenticatorCreateRequest
from dt_api_security_results.models.cloud_authenticators import AwsCredential, CloudAuthenticatorTypesEnum


def test_cloud_resources(api_key):
    client = dt_api_security_results.client.ApiSecurityResultsClient(api_key=api_key)

    # Fetch a few pages of the cloud_resources
    counter = 0
    responses = []
    curr_response = client.cloud_resources_list()
    responses.append(curr_response)
    while curr_response.pagination_information.next_cursor and counter < 4:
        curr_response = client.cloud_resources_list(cursor=curr_response.pagination_information.next_cursor)
        responses.append(curr_response)
        counter += 1

    # Extract information out from the pages
    cloud_resources = []
    pagination_informations = []
    for response in responses:
        cloud_resources.extend(response.cloud_resources)
        pagination_informations.append(response.pagination_information)

    # Check to make sure there aren't duplicates (eg, if we mistakenly return the same page repeatedly)
    assert len({cloud_resource.id for cloud_resource in cloud_resources}) == len(cloud_resources)

    # Check the pagination information's total_count
    for pagination_information in pagination_informations:
        assert len(cloud_resources) <= pagination_information.total_count

    # Try to fetch a sampling of the cloud_resources
    for cloud_resource in [page.cloud_resources[0] for page in responses]:
        get_response = client.cloud_resources_get(cloud_resource.id)
        # The "get" version contains extra data, but sanity check the IDs
        assert get_response.cloud_resources[0].id == cloud_resource.id


def test_ip_ranges(api_key):
    client = dt_api_security_results.client.ApiSecurityResultsClient(api_key=api_key)

    # Fetch a few pages of the ip_ranges
    counter = 0
    responses = []
    curr_response = client.ip_ranges_list()
    responses.append(curr_response)
    while curr_response.pagination_information.next_cursor and counter < 4:
        curr_response = client.ip_ranges_list(cursor=curr_response.pagination_information.next_cursor)
        responses.append(curr_response)
        counter += 1

    # Extract information out from the pages
    ip_ranges = []
    pagination_informations = []
    for response in responses:
        ip_ranges.extend(response.ip_ranges)
        pagination_informations.append(response.pagination_information)

    # Check to make sure there aren't duplicates (eg, if we mistakenly return the same page repeatedly)
    assert len({ip_range.id for ip_range in ip_ranges}) == len(ip_ranges)

    # Check the pagination information's total_count
    for pagination_information in pagination_informations:
        assert len(ip_ranges) <= pagination_information.total_count

    # Try to fetch a sampling of the ip_ranges
    for page in responses:
        if page.ip_ranges:  # can be empty
            ip_range = page.ip_ranges[0]
            get_response = client.ip_ranges_get(ip_range.id)
            # The "get" version contains extra data, but sanity check the IDs
            assert get_response.ip_ranges[0].id == ip_range.id


def test_network_services(api_key):
    client = dt_api_security_results.client.ApiSecurityResultsClient(api_key=api_key)

    # Fetch a few pages of the cloud_resources
    counter = 0
    responses = []
    curr_response = client.network_services_list()
    responses.append(curr_response)
    while curr_response.pagination_information.next_cursor and counter < 4:
        curr_response = client.network_services_list(cursor=curr_response.pagination_information.next_cursor)
        responses.append(curr_response)
        counter += 1

    # Extract information out from the pages
    network_services = []
    pagination_informations = []
    for response in responses:
        network_services.extend(response.network_services)
        pagination_informations.append(response.pagination_information)

    # Check to make sure there aren't duplicates (eg, if we mistakenly return the same page repeatedly)
    assert len({network_service.id for network_service in network_services}) == len(network_services)

    # Check the pagination information's total_count
    for pagination_information in pagination_informations:
        assert len(network_services) <= pagination_information.total_count

    # Try to fetch a sampling of the network_resources
    for network_service in [page.network_services[0] for page in responses]:
        get_response = client.network_services_get(network_service.id)
        # The "get" version contains extra data, but sanity check the IDs
        assert get_response.network_services[0].id == network_service.id


def test_policy_violations(api_key):
    client = dt_api_security_results.client.ApiSecurityResultsClient(api_key=api_key)

    # Fetch a few pages of the cloud_resources
    counter = 0
    responses = []
    curr_response = client.policy_violations_list()
    responses.append(curr_response)
    while curr_response.pagination_information.next_cursor and counter < 4:
        curr_response = client.policy_violations_list(cursor=curr_response.pagination_information.next_cursor)
        responses.append(curr_response)
        counter += 1

    # Extract information out from the pages
    policy_violations = []
    pagination_informations = []
    for response in responses:
        policy_violations.extend(response.policy_violations)
        pagination_informations.append(response.pagination_information)

    # Check to make sure there aren't duplicates (eg, if we mistakenly return the same page repeatedly)
    assert len({policy_violation.id for policy_violation in policy_violations}) == len(policy_violations)

    # Check the pagination information's total_count
    for pagination_information in pagination_informations:
        assert len(policy_violations) <= pagination_information.total_count

    # Try to fetch a sampling of the policy_violations
    for policy_violation in [page.policy_violations[0] for page in responses]:
        get_response = client.policy_violations_get(policy_violation.id)
        # The "get" version contains extra data, but sanity check the IDs
        assert get_response.id == policy_violation.id


def test_restful_apis(api_key):
    client = dt_api_security_results.client.ApiSecurityResultsClient(api_key=api_key)

    # Fetch a few pages of the cloud_resources
    counter = 0
    responses = []
    curr_response = client.restful_apis_list()
    responses.append(curr_response)
    while curr_response.pagination_information.next_cursor and counter < 4:
        curr_response = client.restful_apis_list(cursor=curr_response.pagination_information.next_cursor)
        responses.append(curr_response)
        counter += 1

    # Extract information out from the pages
    restful_apis = []
    pagination_informations = []
    for response in responses:
        restful_apis.extend(response.restful_apis)
        pagination_informations.append(response.pagination_information)

    # Check to make sure there aren't duplicates (eg, if we mistakenly return the same page repeatedly)
    assert len({restful_api.id for restful_api in restful_apis}) == len(restful_apis)

    # Check the pagination information's total_count
    for pagination_information in pagination_informations:
        assert len(restful_apis) <= pagination_information.total_count

    # Try to fetch a sampling of the restful_apis
    for restful_api in [page.restful_apis[0] for page in responses]:
        get_response = client.restful_apis_get(restful_api.id)
        # The "get" version contains extra data, but sanity check the IDs
        assert get_response.restful_apis[0].id == restful_api.id


def test_web_applications(api_key):
    client = dt_api_security_results.client.ApiSecurityResultsClient(api_key=api_key)

    # Fetch a few pages of the cloud_resources
    counter = 0
    responses = []
    curr_response = client.web_applications_list()
    responses.append(curr_response)
    while curr_response.pagination_information.next_cursor and counter < 4:
        curr_response = client.web_applications_list(cursor=curr_response.pagination_information.next_cursor)
        responses.append(curr_response)
        counter += 1

    # Extract information out from the pages
    web_applications = []
    pagination_informations = []
    for response in responses:
        web_applications.extend(response.web_applications)
        pagination_informations.append(response.pagination_information)

    # Check to make sure there aren't duplicates (eg, if we mistakenly return the same page repeatedly)
    assert len({web_application.id for web_application in web_applications}) == len(web_applications)

    # Check the pagination information's total_count
    for pagination_information in pagination_informations:
        assert len(web_applications) <= pagination_information.total_count

    # Try to fetch a sampling of the web_applications
    for web_application in [page.web_applications[0] for page in responses]:
        get_response = client.web_applications_get(web_application.id)
        # The "get" version contains extra data, but sanity check the IDs
        assert get_response.web_applications[0].id == web_application.id


def test_openapi_definitions(api_key):
    client = dt_api_security_results.client.ApiSecurityResultsClient(api_key=api_key)

    # Fetch a few pages of the cloud_resources
    counter = 0
    responses = []
    curr_response = client.openapi_definitions_list()
    responses.append(curr_response)
    while curr_response.pagination_information.next_cursor and counter < 4:
        curr_response = client.openapi_definitions_list(cursor=curr_response.pagination_information.next_cursor)
        responses.append(curr_response)
        counter += 1

    # Extract information out from the pages
    openapi_definitions = []
    pagination_informations = []
    for response in responses:
        openapi_definitions.extend(response.openapi_definitions)
        pagination_informations.append(response.pagination_information)

    # Check to make sure there aren't duplicates (eg, if we mistakenly return the same page repeatedly)
    assert len({openapi_definition.id for openapi_definition in openapi_definitions}) == len(openapi_definitions)

    # Check the pagination information's total_count
    for pagination_information in pagination_informations:
        assert len(openapi_definitions) <= pagination_information.total_count

    # Try to call create openapi_definition with a badly formatted definition
    with pytest.raises(HTTPError) as excinfo:
        client.openapi_definition_create(data="<bad openapi/>")
        assert excinfo.response.status_code == 422


def test_cloud_authenticators(api_key):
    client = dt_api_security_results.client.ApiSecurityResultsClient(api_key=api_key)

    # Try to call create cloud_authenticator with a badly formatted request
    request = CloudAuthenticatorCreateRequest(
        cloud_authenticator_type=CloudAuthenticatorTypesEnum.AMAZON_WEB_SERVICES,
        aws_credential=AwsCredential(
            role_arn="fake_role_arn",
            external_id="fake_external_id",
        ),
    )
    with pytest.raises(HTTPError) as excinfo:
        client.cloud_authenticator_create(authenticator_request=request)
        assert excinfo.response.status_code == 422
