import os

import pytest


@pytest.fixture
def api_key():
    if "TEST_API_KEY" not in os.environ:
        raise RuntimeError(
            "You must specify an API Key via the TEST_API_KEY environment variable for this test to work"
        )

    return os.environ["TEST_API_KEY"]
