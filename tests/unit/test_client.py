import json
from datetime import datetime
from uuid import uuid4

from dt_api_security_results.client import (
    CloudAuthenticatorCreateRequest,
    CloudAuthenticatorCreateResponse,
    CloudResourceGetResponse,
    CloudResourceListResponse,
    IpRangeGetResponse,
    IpRangeListResponse,
    NetworkServiceGetResponse,
    NetworkServiceListResponse,
    OpenApiDefinitionCreateResponse,
    OpenApiDefinitionsResponse,
    PaginationInformationField,
    PolicyViolationGetResponse,
    PolicyViolationListResponse,
    RestfulApiGetResponse,
    RestfulApiListResponse,
    WebApplicationGetResponse,
    WebApplicationListResponse,
)
from dt_api_security_results.models.assets import (
    ApiOperation,
    CloudResource,
    ConnectivityScanResult,
    DomainName,
    IpRange,
    NetworkService,
    OpenApiDefinition,
    RestfulApi,
    WebApplication,
)
from dt_api_security_results.models.cloud_authenticators import (
    AwsAuthenticatorResponse,
    AwsCredential,
    CloudAuthenticatorStatusEnum,
    CloudAuthenticatorTypesEnum,
)
from dt_api_security_results.models.policy_violations import AffectedAsset, PolicyRule, PolicyRuleType, PolicyViolation


def test_cloud_resources_get(mock_client, requests_mock):
    uuid = uuid4()
    get_response = CloudResourceGetResponse(
        cloud_resources=[
            CloudResource(
                id=uuid4(),
                date_created=datetime.now(),
                discovered_via="FAKE",
                name="cloud_resource1",
                cloud_console_url="http://fake.com",
                asset_type_name="type",
            )
        ]
    )
    requests_mock.get(mock_client._url_for(f"/cloud_resources/{uuid}"), json=json.loads(get_response.json()))
    result = mock_client.cloud_resources_get(uuid)
    assert result == get_response


def test_cloud_resources_list(mock_client, requests_mock):
    list_response = CloudResourceListResponse(
        pagination_information=PaginationInformationField(
            total_count=1,
            next_cursor="abc",
        ),
        cloud_resources=[
            CloudResource(
                id=uuid4(),
                date_created=datetime.now(),
                discovered_via="FAKE",
                name="cloud_resource1",
                cloud_console_url="http://fake.com",
                asset_type_name="type",
            ),
            CloudResource(
                id=uuid4(),
                date_created=datetime.now(),
                discovered_via="FAKE",
                name="cloud_resource2",
                cloud_console_url="http://fake.com",
                asset_type_name="type",
            ),
        ],
    )
    requests_mock.get(mock_client._url_for("/cloud_resources"), json=json.loads(list_response.json()))
    result = mock_client.cloud_resources_list()
    assert result == list_response


def test_ip_ranges_get(mock_client, requests_mock):
    uuid = uuid4()
    get_response = IpRangeGetResponse(
        ip_ranges=[
            IpRange(
                id=uuid4(),
                ip_range="8.0.0.0/24",
                has_continuous_discovery_enabled=True,
            )
        ]
    )
    requests_mock.get(mock_client._url_for(f"/ip_ranges/{uuid}"), json=json.loads(get_response.json()))
    result = mock_client.ip_ranges_get(uuid)
    assert result == get_response


def test_ip_ranges_list(mock_client, requests_mock):
    list_response = IpRangeListResponse(
        pagination_information=PaginationInformationField(
            total_count=1,
            next_cursor="abc",
        ),
        ip_ranges=[
            IpRange(
                id=uuid4(),
                ip_range="8.0.0.0/24",
                has_continuous_discovery_enabled=True,
            ),
            IpRange(
                id=uuid4(),
                ip_range="8.0.1.0/24",
                has_continuous_discovery_enabled=True,
            ),
        ],
    )
    requests_mock.get(mock_client._url_for("/ip_ranges"), json=json.loads(list_response.json()))
    result = mock_client.ip_ranges_list()
    assert result == list_response


def test_ip_ranges_create(mock_client, requests_mock):
    get_response = IpRangeGetResponse(
        ip_ranges=[
            IpRange(
                id=uuid4(),
                ip_range="8.0.0.0/24",
                has_continuous_discovery_enabled=True,
            )
        ]
    )
    requests_mock.post(mock_client._url_for("/ip_ranges"), json=json.loads(get_response.json()))
    result = mock_client.ip_ranges_create("8.0.0.0/24")
    assert result == get_response


def test_network_services_get(mock_client, requests_mock):
    uuid = uuid4()
    get_response = NetworkServiceGetResponse(
        domain_names=[
            DomainName(
                id=uuid4(),
                name="domain_name1",
                date_created=datetime.now(),
            )
        ],
        network_services=[
            NetworkService(
                id=uuid4(),
                date_created=datetime.now(),
                domain_name_id=uuid4(),
                port=80,
                application_layer_protocol="protocol",
                is_tls_encrypted=False,
                url="http://fake.com",
                discovered_via="FAKE",
                connectivity_scan_results=[
                    ConnectivityScanResult(status="ConnectivityStatusEnum", date_created=datetime.now())
                ],
            )
        ],
    )
    requests_mock.get(mock_client._url_for(f"/network_services/{uuid}"), json=json.loads(get_response.json()))
    result = mock_client.network_services_get(uuid)
    assert result == get_response


def test_network_services_list(mock_client, requests_mock):
    list_response = NetworkServiceListResponse(
        pagination_information=PaginationInformationField(
            total_count=1,
            next_cursor="abc",
        ),
        domain_names=[
            DomainName(
                id=uuid4(),
                name="domain_name1",
                date_created=datetime.now(),
            )
        ],
        network_services=[
            NetworkService(
                id=uuid4(),
                date_created=datetime.now(),
                domain_name_id=uuid4(),
                port=80,
                application_layer_protocol="protocol",
                is_tls_encrypted=False,
                url="http://fake.com",
                discovered_via="FAKE",
                connectivity_scan_results=[
                    ConnectivityScanResult(status="ConnectivityStatusEnum", date_created=datetime.now())
                ],
            )
        ],
    )
    requests_mock.get(mock_client._url_for("/network_services"), json=json.loads(list_response.json()))
    result = mock_client.network_services_list()
    assert result == list_response


def test_policy_violations_get(mock_client, requests_mock):
    uuid = uuid4()
    get_response = PolicyViolationGetResponse(
        id=uuid4(),
        status="PolicyViolationStatusEnum",
        date_created=datetime.now(),
        violated_policy_rule=PolicyRule(
            id=uuid4(),
            status="PolicyRuleStatusEnum",
            policy_rule_type=PolicyRuleType(
                id=uuid4(),
                title="title",
                description="description",
                recommendation="recommendation",
                compliance_policy_references=[],
            ),
            severity="RelevanceEnum",
        ),
        affected_asset=AffectedAsset(
            id=uuid4(),
            name="asset name",
            hosted_on="CloudProviderEnum",
            date_created=datetime.now(),
            asset_type="AssetTypeEnum",
            asset_type_name="",
            asset_type_icon_url="",
        ),
    )
    requests_mock.get(
        mock_client._url_for(f"/policy_violations/{uuid}", api_version="v2"), json=json.loads(get_response.json())
    )
    result = mock_client.policy_violations_get(uuid)
    assert result == get_response


def test_policy_violations_list(mock_client, requests_mock):
    list_response = PolicyViolationListResponse(
        pagination_information=PaginationInformationField(
            total_count=1,
            next_cursor="abc",
        ),
        policy_violations=[
            PolicyViolation(
                id=uuid4(),
                status="PolicyViolationStatusEnum",
                date_created=datetime.now(),
                violated_policy_rule=PolicyRule(
                    id=uuid4(),
                    status="PolicyRuleStatusEnum",
                    policy_rule_type=PolicyRuleType(
                        id=uuid4(),
                        title="title",
                        description="description",
                        recommendation="recommendation",
                        compliance_policy_references=[],
                    ),
                    severity="SeverityEnum",
                ),
                affected_asset=AffectedAsset(
                    id=uuid4(),
                    name="asset name",
                    hosted_on="CloudProviderEnum",
                    date_created=datetime.now(),
                    asset_type="AssetTypeEnum",
                    asset_type_name="",
                    asset_type_icon_url="",
                ),
            )
        ],
    )
    requests_mock.get(
        mock_client._url_for("/policy_violations", api_version="v2"), json=json.loads(list_response.json())
    )
    result = mock_client.policy_violations_list()
    assert result == list_response


def test_restful_apis_get(mock_client, requests_mock):
    uuid = uuid4()
    get_response = RestfulApiGetResponse(
        restful_apis=[
            RestfulApi(
                id=uuid4(),
                date_created=datetime.now(),
                network_service_id=uuid4(),
                title="str",
                base_url="str",
                base_path="str",
                api_operations=[
                    ApiOperation(
                        id=uuid4(),
                        date_created=datetime.now(),
                        restful_api_id=uuid4(),
                        path="/path",
                        http_method="HttpMethodEnum",
                        is_authenticated=True,
                    )
                ],
            )
        ],
        domain_names=[
            DomainName(
                id=uuid4(),
                name="domain_name1",
                date_created=datetime.now(),
            )
        ],
        network_services=[
            NetworkService(
                id=uuid4(),
                date_created=datetime.now(),
                domain_name_id=uuid4(),
                port=80,
                application_layer_protocol="protocol",
                is_tls_encrypted=False,
                url="http://fake.com",
                discovered_via="FAKE",
                connectivity_scan_results=[
                    ConnectivityScanResult(status="ConnectivityStatusEnum", date_created=datetime.now())
                ],
            )
        ],
    )
    requests_mock.get(mock_client._url_for(f"/restful_apis/{uuid}"), json=json.loads(get_response.json()))
    result = mock_client.restful_apis_get(uuid)
    assert result == get_response


def test_restful_apis_list(mock_client, requests_mock):
    list_response = RestfulApiListResponse(
        pagination_information=PaginationInformationField(
            total_count=1,
            next_cursor="abc",
        ),
        restful_apis=[
            RestfulApi(
                id=uuid4(),
                date_created=datetime.now(),
                network_service_id=uuid4(),
                title="str",
                base_url="str",
                base_path="str",
                api_operations=[
                    ApiOperation(
                        id=uuid4(),
                        date_created=datetime.now(),
                        restful_api_id=uuid4(),
                        path="/path",
                        http_method="HttpMethodEnum",
                        is_authenticated=True,
                    )
                ],
            )
        ],
        domain_names=[
            DomainName(
                id=uuid4(),
                name="domain_name1",
                date_created=datetime.now(),
            )
        ],
        network_services=[
            NetworkService(
                id=uuid4(),
                date_created=datetime.now(),
                domain_name_id=uuid4(),
                port=80,
                application_layer_protocol="protocol",
                is_tls_encrypted=False,
                url="http://fake.com",
                discovered_via="FAKE",
                connectivity_scan_results=[
                    ConnectivityScanResult(status="ConnectivityStatusEnum", date_created=datetime.now())
                ],
            )
        ],
    )
    requests_mock.get(mock_client._url_for("/restful_apis"), json=json.loads(list_response.json()))
    result = mock_client.restful_apis_list()
    assert result == list_response


def test_web_applications_get(mock_client, requests_mock):
    uuid = uuid4()
    get_response = WebApplicationGetResponse(
        web_applications=[
            WebApplication(
                id=uuid4(),
                date_created=datetime.now(),
                discovered_via="DiscoveredViaEnum",
                base_url="http://fake.com",
                base_path="/",
                network_service_id=uuid4(),
            )
        ],
        domain_names=[
            DomainName(
                id=uuid4(),
                name="domain_name1",
                date_created=datetime.now(),
            )
        ],
        network_services=[
            NetworkService(
                id=uuid4(),
                date_created=datetime.now(),
                domain_name_id=uuid4(),
                port=80,
                application_layer_protocol="protocol",
                is_tls_encrypted=False,
                url="http://fake.com",
                discovered_via="FAKE",
                connectivity_scan_results=[
                    ConnectivityScanResult(status="ConnectivityStatusEnum", date_created=datetime.now())
                ],
            )
        ],
    )
    requests_mock.get(mock_client._url_for(f"/web_applications/{uuid}"), json=json.loads(get_response.json()))
    result = mock_client.web_applications_get(uuid)
    assert result == get_response


def test_web_applications_list(mock_client, requests_mock):
    get_response = WebApplicationListResponse(
        pagination_information=PaginationInformationField(
            total_count=1,
            next_cursor="abc",
        ),
        web_applications=[
            WebApplication(
                id=uuid4(),
                date_created=datetime.now(),
                discovered_via="DiscoveredViaEnum",
                base_url="http://fake.com",
                base_path="/",
                network_service_id=uuid4(),
            )
        ],
        domain_names=[
            DomainName(
                id=uuid4(),
                name="domain_name1",
                date_created=datetime.now(),
            )
        ],
        network_services=[
            NetworkService(
                id=uuid4(),
                date_created=datetime.now(),
                domain_name_id=uuid4(),
                port=80,
                application_layer_protocol="protocol",
                is_tls_encrypted=False,
                url="http://fake.com",
                discovered_via="FAKE",
                connectivity_scan_results=[
                    ConnectivityScanResult(status="ConnectivityStatusEnum", date_created=datetime.now())
                ],
            )
        ],
    )
    requests_mock.get(mock_client._url_for("/web_applications"), json=json.loads(get_response.json()))
    result = mock_client.web_applications_list()
    assert result == get_response


def test_openapi_definitions_list(mock_client, requests_mock):
    get_response = OpenApiDefinitionsResponse(
        pagination_information=PaginationInformationField(
            total_count=1,
            next_cursor="abc",
        ),
        openapi_definitions=[
            OpenApiDefinition(
                id=uuid4(),
                date_created=datetime.now(),
                title="title",
                original_document="original_document",
                status="status",
                discovered_via="FAKE",
            )
        ],
    )
    requests_mock.get(mock_client._url_for("/openapi_definitions"), json=json.loads(get_response.json()))
    result = mock_client.openapi_definitions_list()
    assert result == get_response


def test_openapi_definition_create(mock_client, requests_mock):
    get_response = OpenApiDefinitionCreateResponse(
        id=uuid4(),
        network_services=[
            NetworkService(
                id=uuid4(),
                date_created=datetime.now(),
                domain_name_id=uuid4(),
                port=80,
                application_layer_protocol="protocol",
                is_tls_encrypted=False,
                url="http://fake.com",
                discovered_via="FAKE",
                connectivity_scan_results=[
                    ConnectivityScanResult(status="ConnectivityStatusEnum", date_created=datetime.now())
                ],
            )
        ],
        domain_names=[
            DomainName(
                id=uuid4(),
                name="domain_name1",
                date_created=datetime.now(),
            )
        ],
        restful_apis=[
            RestfulApi(
                id=uuid4(),
                date_created=datetime.now(),
                network_service_id=uuid4(),
                title="str",
                base_url="str",
                base_path="str",
                api_operations=[
                    ApiOperation(
                        id=uuid4(),
                        date_created=datetime.now(),
                        restful_api_id=uuid4(),
                        path="/path",
                        http_method="HttpMethodEnum",
                        is_authenticated=True,
                    )
                ],
            )
        ],
    )
    requests_mock.post(mock_client._url_for("/openapi_definitions"), json=json.loads(get_response.json()))
    result = mock_client.openapi_definition_create(data="openapi_definition")
    assert result == get_response


def test_cloud_authenticator_create(mock_client, requests_mock):
    get_response = CloudAuthenticatorCreateResponse(
        status=CloudAuthenticatorStatusEnum.ONLINE,
        aws_credential=AwsAuthenticatorResponse(
            id=str(uuid4()),
            added_by_actor_id=str(uuid4()),
            added_by_actor_name="test",
            added_by_user_id=str(uuid4()),
            date_created=datetime.now(),
            role_arn="fake_role_arn",
            external_id="fake_external_id",
        ),
        authenticator_type=CloudAuthenticatorTypesEnum.AMAZON_WEB_SERVICES,
    )
    requests_mock.post(mock_client._url_for("/cloud_authenticators"), json=json.loads(get_response.json()))
    request = CloudAuthenticatorCreateRequest(
        cloud_authenticator_type=CloudAuthenticatorTypesEnum.AMAZON_WEB_SERVICES,
        aws_credential=AwsCredential(
            role_arn="fake_role_arn",
            external_id="fake_external_id",
        ),
    )
    result = mock_client.cloud_authenticator_create(authenticator_request=request)
    assert result == get_response
