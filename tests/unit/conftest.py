import pytest

import dt_api_security_results.client


@pytest.fixture
def mock_client():
    fake_api_key = "fake_key"
    return dt_api_security_results.client.ApiSecurityResultsClient(api_key=fake_api_key)
