# Data Theorem API / Cloud / Web Secure Client

This project is the reference client for Data Theorem's API / Cloud / Web Secure.

It is currently in a beta state of development and more feature/model fields will be added in the future.

For documentation of the API itself, visit: <https://docs.securetheorem.com/api_security_results/overview.html>

## Requirements

The client requires Python 3.6 or newer. If you have both Python 2.7 and Python 3.x installed, you may need to adjust
the commands mentioned here to use `python3`, `pip3`, etc.

## Installation

First, clone the repository:

```bash
git clone git@bitbucket.org:datatheorem/dt-api-security-results.git
cd dt-api-security-results
```

Once it is done, you can install it for just your local user with pip like:

```bash
pip install .
```

## Usage


### API client

The main class to use is `dt_api_security_results.client.ApiSecurityResultsClient`, which takes an `api_key` argument:

```python
from dt_api_security_results.client import ApiSecurityResultsClient

client = ApiSecurityResultsClient(api_key="your-api-key")
response = client.policy_violations_list()
```

The client defines the various methods for fetching data from the Data Theorem portal which are all documented in the
`client.py` file.

**Examples scripts are available in the ./examples folder.**

### Example: Export Urgent Violations to XLS

The `export_urgent_violations_to_xls.py` script will retrieve all URGENT violations that are currently marked open in
Data Theorem's API Inspect portal. It will export them to an Excel spreadsheet.

If you want to try using it, you need to also install some extra dependencies that aren't needed by the client itself.
The "examples" flag will install them for you. Eg:

```bash
pip install  ".[examples]"
```

To run the command, you need to create an API Key within the Data Theorem portal, grant it access to the **API Security
Results API**, and then pass it in to the command.

```bash
python export_urgent_violations_to_xls.py $API_KEY
```

This will create a file called `urgent_violations.xls`.


### Example: Export Assets from your Inventory to XLS

The `export_assets_to_xls.py` script will retrieve all assets that are currently marked with specified asset tags in
Data Theorem's API Inspect portal. It will export them to an Excel spreadsheet.

If you want to try using it, you need to also install some extra dependencies that aren't needed by the client itself.
The "examples" flag will install them for you. Eg:

```bash
pip install  ".[examples]"
```

To run the command, you need to create an API Key within the Data Theorem portal, grant it access to the **API Security
Results API**, and then pass it in to the command.

Note: You'll need to modify the filters inside the .py file in order to get the subset of the assets, for example,
to get assets with applies asset tag "asset-id: 1234" you'll need to have filter_by_asset_tags={"asset-id": "1234"} in
the call to the `retrieve_assets` call in the "main" function. Look for "# MODIFY FILTERS HERE" line within the file

```bash
python export_assets_to_xls.py $API_KEY
```

This will create a file called `exported_assets.xls`.
