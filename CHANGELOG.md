# dt-api-security-results Changelog

## 0.6.0 - 2022/07/25

* Update policy_violations API calls to v2
* Add new column "Additional Information" to export urgent violations xls
* Refactor column “Relevance” to “Severity” in export urgent violations xls

## 0.5.0 - 2022/03/07

* Add new columns "Asset URL" and "Asset AWS ARN" to export urgent violations xls

## 0.4.0 - 2021/01/14

* Add CloudAuthenticator POST support
* Fix data validation for CloudResource 

## 0.3.0 - 2020/05/14

* Add IP range GET/POST support

## 0.2.0 - 2020/03/18

* Major rework/rename of the client package, main client class, etc.
* Ensure all list operations support pagination arguments
* Add integration tests to ensure the client can talk to the API (Requires an API Key)
* Run all tests/linters on Pythons 3.6, 3.7, and 3.8
